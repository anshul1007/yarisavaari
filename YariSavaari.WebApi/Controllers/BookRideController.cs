﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using YariSavaari.BusinessEntity;
using YariSavaari.BusinessService;
using YariSavaari.DataModel;
using YariSavaari.DataModel.Repository;
using YariSavaari.WebApi.Models;

namespace YariSavaari.WebApi.Controllers
{
    //[Authorize]
    public class BookRideController : ApiController
    {

        private readonly IBookRideServices _BookRideServices;

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize BookRide service instance
        /// </summary>
        public BookRideController()
        {
            _BookRideServices = new BookRideServices();
        }

        #endregion

        //GET api/BookRide
        public HttpResponseMessage Get()
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

            var userId = identity.Claims.Where(c => c.Type == "UserId")
                   .Select(c => c.Value).SingleOrDefault();

            var shareRides = _BookRideServices.PickPassenger(userId);
            if (shareRides != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, shareRides);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "BookRides not found");
        }

        //POST api/bookride
        [Authorize]
        [ActionName("Create")]
        public HttpResponseMessage Post([FromBody] CreateBookRideBindingModel bookRideModel)
        {
            try
            {
                if (bookRideModel.Location == null)
                {
                    throw new Exception("BookRide Model is incorrect");
                }

                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

                var userId = identity.Claims.Where(c => c.Type == "UserId")
                       .Select(c => c.Value).SingleOrDefault();

                PathGeneration.CalculateTime(bookRideModel.Location, bookRideModel.StartAt);

                var path = PathGeneration.GeneratePath(bookRideModel.Location);


                return Request.CreateResponse(HttpStatusCode.OK, _BookRideServices.CreateBookRide(path, bookRideModel.StartAt, userId));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}