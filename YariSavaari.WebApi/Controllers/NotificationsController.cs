﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using YariSavaari.BusinessEntity;
using YariSavaari.BusinessService;

namespace YariSavaari.WebApi.Controllers
{
    public class NotificationsController : ApiController
    {
        private readonly INotificationServices _NotificationServices;

        public NotificationsController()
        {
            _NotificationServices = new NotificationServices();
        }

        [Authorize]
        public HttpResponseMessage Get()
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

            var userId = identity.Claims.Where(c => c.Type == "UserId")
                   .Select(c => c.Value).SingleOrDefault();

            var bookRides = _NotificationServices.GetAllNotifications(userId);
            if (bookRides != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, bookRides);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "ShareRides not found");
        }

        [Authorize]
        [ActionName("ShareRequest")]
        public HttpResponseMessage PostShareRequest(CoRideDetailsEntity ride)
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

            var userId = identity.Claims.Where(c => c.Type == "UserId")
                   .Select(c => c.Value).SingleOrDefault();

            var status = _NotificationServices.ShareRequest(userId, ride);
            return Request.CreateResponse(HttpStatusCode.OK, status);
        }
    }
}
