﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using YariSavaari.BusinessEntity;
using YariSavaari.BusinessService;
using YariSavaari.DataModel;
using YariSavaari.DataModel.Repository;

namespace YariSavaari.WebApi.Controllers
{
    [Authorize]
    public class VehiclesController : ApiController
    {

        private readonly IVehicleServices _vehicleServices;

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize vehicle service instance
        /// </summary>
        public VehiclesController()
        {
            _vehicleServices = new VehicleServices();
        }

        //public VehiclesController(IVehicleServices vehicleServices)
        //{
        //    _vehicleServices = vehicleServices;
        //}
        #endregion

        //GET api/vehicle
        public HttpResponseMessage Get()
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

            var userId = identity.Claims.Where(c => c.Type == "UserId")
                   .Select(c => c.Value).SingleOrDefault();

            var vehicles = _vehicleServices.GetAllVehicles(userId);
            if (vehicles != null)
            {
                var vehicleEntities = vehicles as List<VehicleEntity> ?? vehicles.ToList();
                if (vehicleEntities.Any())
                    return Request.CreateResponse(HttpStatusCode.OK, vehicleEntities);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Vehicles not found");
        }

        // GET api/vehicle/5
        public HttpResponseMessage Get(int id)
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

            var userId = identity.Claims.Where(c => c.Type == "UserId")
                   .Select(c => c.Value).SingleOrDefault();

            var vehicles = _vehicleServices.GetAllVehicles(userId);

            var vehicle = _vehicleServices.GetVehicleById(id, userId);
            if (vehicle != null)
                return Request.CreateResponse(HttpStatusCode.OK, vehicle);
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No vehicle found for this id");
        }

        //POST api/vehicle
        public HttpResponseMessage Post([FromBody] VehicleEntity vehicleEntity)
        {
            try
            {
                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

                var userId = identity.Claims.Where(c => c.Type == "UserId")
                       .Select(c => c.Value).SingleOrDefault();

                vehicleEntity.UserId = userId;

                return Request.CreateResponse(HttpStatusCode.OK, _vehicleServices.CreateVehicle(vehicleEntity));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // PUT api/vehicle/5
        public bool Put(int id, [FromBody]VehicleEntity vehicleEntity)
        {
            if (id > 0)
            {
                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

                var userId = identity.Claims.Where(c => c.Type == "UserId")
                       .Select(c => c.Value).SingleOrDefault();

                return _vehicleServices.UpdateVehicle(id, vehicleEntity, userId);
            }
            return false;
        }

        // DELETE api/vehicle/5
        //public bool Delete(int id)
        //{
        //    if (id > 0)
        //        return _vehicleServices.DeleteVehicle(id);
        //    return false;
        //}
    }
}