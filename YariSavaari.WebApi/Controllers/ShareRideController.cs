﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using YariSavaari.BusinessEntity;
using YariSavaari.BusinessService;
using YariSavaari.WebApi.Models;

namespace YariSavaari.WebApi.Controllers
{

    public class ShareRideController : ApiController
    {

        private readonly IShareRideServices _ShareRideServices;

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize ShareRide service instance
        /// </summary>
        public ShareRideController()
        {
            _ShareRideServices = new ShareRideServices();
        }

        //public ShareRidesController(IShareRideServices ShareRideServices)
        //{
        //    _ShareRideServices = ShareRideServices;
        //}
        #endregion

        //GET api/ShareRide
        [Authorize]
        public HttpResponseMessage Get()
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

            var userId = identity.Claims.Where(c => c.Type == "UserId")
                   .Select(c => c.Value).SingleOrDefault();

            var bookRides = _ShareRideServices.PickPassenger(userId);
            if (bookRides != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, bookRides);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "ShareRides not found");
        }

        //POST api/shareride
        [Authorize]
        [ActionName("Create")]
        public HttpResponseMessage Post([FromBody] CreateShareRideBindingModel shareRideModel)
        {
            try
            {
                if (shareRideModel.Location == null)
                {
                    throw new Exception("ShareRide Model is incorrect");
                }

                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

                var userId = identity.Claims.Where(c => c.Type == "UserId")
                       .Select(c => c.Value).SingleOrDefault();

                PathGeneration.CalculateTime(shareRideModel.Location, shareRideModel.StartAt);

                var path = PathGeneration.GeneratePath(shareRideModel.Location);

                return Request.CreateResponse(HttpStatusCode.OK, _ShareRideServices.CreateShareRide(path, shareRideModel.StartAt, shareRideModel.VehicleId, userId));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}