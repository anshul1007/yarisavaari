﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using YariSavaari.BusinessEntity;

namespace YariSavaari.WebApi.Models
{
    public class CreateShareRideBindingModel
    {
        public List<LocationEntity> Location { get; set; }
        public DateTime StartAt { get; set; }
        public int VehicleId { get; set; }
    }
}