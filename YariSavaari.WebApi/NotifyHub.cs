﻿using System;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace YariSavaari.WebApi
{
    public class NotifyHub : Hub
    {
        List<string> ConnectedUsers = new List<string>();
        public void Send(int note)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastMessage(note);
        }

        public void connect(string userName)
        {
            if (!ConnectedUsers.Any(x => x == userName))
            {
                ConnectedUsers.Add(userName);
            }
        }

        //public override Task OnConnected()
        //{
        //    var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

        //    var userId = identity.Claims.Where(c => c.Type == "UserId")
        //           .Select(c => c.Value).SingleOrDefault();

        //    if (!ConnectedUsers.Any(x => x == userId))
        //    {
        //        ConnectedUsers.Add(userId);
        //    }

        //    return base.OnConnected();
        //}
    }
}