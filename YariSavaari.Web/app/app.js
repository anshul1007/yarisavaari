﻿
var app = angular.module('AngularAuthApp', ['ngRoute', 'LocalStorageModule', 'angular-loading-bar', 'autoComplete', 'ngAutocomplete', 'ui.bootstrap.datetimepicker', 'angularMoment']);

app.config(function ($routeProvider) {

    $routeProvider.when("/home", {
        controller: "homeController",
        templateUrl: "/app/views/home.html"
    });

    $routeProvider.when("/login", {
        controller: "loginController",
        templateUrl: "/app/views/login.html"
    });

    $routeProvider.when("/signup", {
        controller: "signupController",
        templateUrl: "/app/views/signup.html"
    });

    $routeProvider.when("/bookride", {
        controller: "bookRideController",
        templateUrl: "/app/views/bookride.html"
    });

    $routeProvider.when("/viewbookride", {
        controller: "bookRideController",
        templateUrl: "/app/views/viewbookride.html"
    });

    $routeProvider.when("/shareride", {
        controller: "shareRideController",
        templateUrl: "/app/views/shareride.html"
    });

    $routeProvider.when("/viewshareride", {
        controller: "shareRideController",
        templateUrl: "/app/views/viewshareride.html"
    });

    $routeProvider.when("/addvehicle", {
        controller: "vehicleController",
        templateUrl: "/app/views/addvehicle.html"
    });

    $routeProvider.when("/viewvehicle", {
        controller: "vehicleController",
        templateUrl: "/app/views/viewvehicle.html"
    });

    $routeProvider.when("/notifications", {
        controller: "notificationsController",
        templateUrl: "/app/views/notifications.html"
    });
    //$routeProvider.when("/orders", {
    //    controller: "ordersController",
    //    templateUrl: "/app/views/orders.html"
    //});

    //$routeProvider.when("/refresh", {
    //    controller: "refreshController",
    //    templateUrl: "/app/views/refresh.html"
    //});

    //$routeProvider.when("/tokens", {
    //    controller: "tokensManagerController",
    //    templateUrl: "/app/views/tokens.html"
    //});

    //$routeProvider.when("/associate", {
    //    controller: "associateController",
    //    templateUrl: "/app/views/associate.html"
    //});

    $routeProvider.otherwise({ redirectTo: "/home" });

});

//var serviceBase = 'http://anshul1007-001-site1.myasp.net/';
var serviceBase = 'http://localhost:802/';
app.constant('ngAuthSettings', {
    apiServiceBaseUri: serviceBase,
    clientId: 'ngAuthApp'
});

app.constant('ngStatus', {
    SEND: { value: 1, name: "Send" },
    SENT: { value: 2, name: "Sent" },
    PENDING: { value: 3, name: "Pending" },
    ACCEPTED: { value: 4, name: "Accepted" },
    DECLINED: { value: 5, name: "Declined" },
    CANCELED: { value: 6, name: "Canceled" }
});

app.value('$', $);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

app.run(['authService', function (authService) {
    authService.fillAuthData();
}]);


