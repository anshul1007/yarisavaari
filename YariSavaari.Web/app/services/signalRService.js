﻿'use strict';
app.service('signalRService', ['$', '$rootScope', function ($, $rootScope) {
    var proxy;
    var connection;
    return {
        connect: function (userName) {
            //$.connection.hub.url = "http://localhost:802/signalr";
            connection = $.hubConnection("http://localhost:802/signalr");
            proxy = connection.createHubProxy('NotifyHub');
            //connection.start();
            connection.start().done(function () {
                proxy.invoke('connect', userName);
            }).fail(function (error) {
                console.error(error);
            });
            proxy.on('broadcastMessage', function (note) {
                $rootScope.$broadcast('broadcastMessage', note);
            });
        },
        isConnecting: function () {
            return connection.state === 0;
        },
        isConnected: function () {
            return connection.state === 1;
        },
        connectionState: function () {
            return connection.state;
        },
        send: function (note) {
            proxy.invoke('Send', note);
        },
    }
}]);

