﻿'use strict';
app.factory('bookRideService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var bookRideServiceFactory = {};

    var _saveBookRide = function (locations, startAt) {
        var inputData = { location: locations, startAt: startAt };
        return $http.post(serviceBase + 'api/bookride', inputData).then(function (response) {
            return response;
        });

    };

    var _getBookRide = function () {
        return $http.get(serviceBase + 'api/bookride').then(function (response) {
            return response;
        });

    };

    bookRideServiceFactory.saveBookRide = _saveBookRide;
    bookRideServiceFactory.getBookRide = _getBookRide;

    return bookRideServiceFactory;

}]);