﻿'use strict';
app.factory('vehiclesService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var vehiclesServiceFactory = {};

    var _getVehicles = function () {

        return $http.get(serviceBase + 'api/vehicles').then(function (results) {
            return results;
        });
    };

    var _saveVehicle = function (vehicle) {

        return $http.post(serviceBase + 'api/vehicles', vehicle).then(function (response) {
            return response;
        });

    };

    vehiclesServiceFactory.getVehicles = _getVehicles;
    vehiclesServiceFactory.saveVehicle = _saveVehicle;

    return vehiclesServiceFactory;

}]);