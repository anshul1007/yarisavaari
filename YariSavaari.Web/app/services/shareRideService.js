﻿'use strict';
app.factory('shareRideService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var shareRideServiceFactory = {};

    var _saveShareRide = function (locations, startAt, vehicleId) {
        var inputData = { location: locations, startAt: startAt, vehicleId: vehicleId };
        return $http.post(serviceBase + 'api/shareride/create', inputData).then(function (response) {
            return response;
        });

    };

    var _getShareRide = function () {
        return $http.get(serviceBase + 'api/shareride').then(function (response) {
            return response;
        });

    };

    var _sendRequest = function (shareRideId, bookRideId) {
        var inputData = { senderRideId: shareRideId, RecipientRideId: bookRideId, RideType: 1, RideStatus: 2 };
        return $http.post(serviceBase + 'api/shareride/sharerequest', inputData).then(function (response) {
            return response;
        });
    }

    shareRideServiceFactory.saveShareRide = _saveShareRide;
    shareRideServiceFactory.getShareRide = _getShareRide;
    shareRideServiceFactory.sendRequest = _sendRequest;

    return shareRideServiceFactory;

}]);