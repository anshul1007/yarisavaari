﻿'use strict';
app.factory('notificationsService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var notificationsServiceFactory = {};

    var _getNotifications = function () {
        return $http.get(serviceBase + 'api/notifications').then(function (response) {
            return response;
        });

    };

    var _sendRequest = function (shareRideId, bookRideId, rideType, status) {
        var inputData = { ShareRideId: shareRideId, BookRideId: bookRideId, RideType: rideType, RideStatus: status };
        return $http.post(serviceBase + 'api/notifications/sharerequest', inputData).then(function (response) {
            return response;
        });
    }

    notificationsServiceFactory.getNotifications = _getNotifications;
    notificationsServiceFactory.sendRequest = _sendRequest;

    return notificationsServiceFactory;

}]);