﻿'use strict';
app.controller('shareRideController', ['$scope', '$location', '$timeout', 'shareRideService', 'vehiclesService', 'notificationsService', 'ngStatus',
    function ($scope, $location, $timeout, shareRideService, vehiclesService, notificationsService, ngStatus) {

        var addMinutes = function (date, minutes) {
            return new Date(date.getTime() + minutes * 60000);
        };

        var varifyLocations = function (locations) {
            for (var index in locations) {
                var location = locations[index];
                if (typeof (location.name) === 'undefined' || location.name === '' ||
                    typeof (location.longitude) === 'undefined' || location.longitude === '' ||
                    typeof (location.latitude) === 'undefined' || location.latitude === '') {

                    return false;
                }
            }
            return true;
        }

        $scope.savedSuccessfully = false;
        $scope.message = "";
        $scope.shareRideCollection = [];
        $scope.shareRide = {};
        $scope.Status = ngStatus;

        $scope.shareRide.startAt = {
            date: moment(addMinutes(new Date(), 15)).format('YYYY-MM-DD HH:mm'),
            active: false,
            past: false
        };

        if ($location.path() === '/viewshareride') {
            shareRideService.getShareRide().then(function (results) {
                $scope.shareRideCollection = results.data;
                if ($scope.shareRideCollection == null || $scope.shareRideCollection.length == 0) {
                    $location.path('/shareride');
                }
            }, function (error) {
                //alert(error.data.message);
            });
        }

        var locations = [];

        var location = {
            "locationType": 1,
            "name": "",
            "longitude": "",
            "latitude": "",
        };

        locations.push(location);

        var location = {
            "locationType": 3,
            "name": "",
            "longitude": "",
            "latitude": "",
        };

        locations.push(location);

        var location = {
            "locationType": 3,
            "name": "",
            "longitude": "",
            "latitude": "",
        };

        locations.push(location);

        var location = {
            "locationType": 3,
            "name": "",
            "longitude": "",
            "latitude": "",
        };

        locations.push(location);

        var location = {
            "locationType": 2,
            "name": "",
            "longitude": "",
            "latitude": "",
        };

        locations.push(location);
        $scope.shareRide.locations = locations;

        vehiclesService.getVehicles().then(function (results) {
            $scope.vehicles = results.data;
            if ($scope.vehicles == null || $scope.vehicles.length == 0) {
                $location.path('/addvehicle');
            }

        }, function (error) {
            $location.path('/addvehicle');
        });

        $scope.getStatus = function (id) {
            for (var val in $scope.Status) {
                if ($scope.Status[val].value == id) {
                    return $scope.Status[val].name;
                }
            }
        };

        $scope.saveShareRide = function () {

            if ($scope.selectedVehicle && $scope.selectedVehicle.vehicleId) {
                if (varifyLocations($scope.shareRide.locations)) {

                    var startAt = moment($scope.shareRide.startAt.date).format()

                    shareRideService.saveShareRide($scope.shareRide.locations, startAt, $scope.selectedVehicle.vehicleId).then(function (response) {

                        $scope.savedSuccessfully = true;
                        $scope.message = "Ride has been added successfully, you will be redicted in 2 seconds.";
                        startTimer();

                    },
                     function (response) {
                         var errors = [];
                         for (var key in response.data.modelState) {
                             for (var i = 0; i < response.data.modelState[key].length; i++) {
                                 errors.push(response.data.modelState[key][i]);
                             }
                         }
                         $scope.message = "Failed to register user due to:" + errors.join(' ');
                     });
                }
                else {
                    $scope.message = "Please select all the locations";
                }
            }
            else {
                $scope.message = "Please select the vehicle";
            }
        };

        var updateStatus = function (status, shareRideId, bookRideId) {
            for (var shareRide in $scope.shareRideCollection) {
                if ($scope.shareRideCollection[shareRide].senderId === shareRideId) {
                    var rideCollection = $scope.shareRideCollection[shareRide].rideSuggestionEntity;
                    for (var rideId in rideCollection) {
                        if (rideCollection[rideId].rideId === bookRideId) {
                            rideCollection[rideId].rideStatus = status;
                        }
                    }
                }
            }
        }

        $scope.action = function (status, shareRideId, bookRideId) {
            switch (status) {
                case $scope.Status.SEND.value:
                    notificationsService.sendRequest(shareRideId, bookRideId, 1, $scope.Status.SENT.value).then(function (results) {
                        if (results.data) {
                            updateStatus($scope.Status.SENT.value, shareRideId, bookRideId);
                        }
                    }, function (error) {
                    });
                    break;
                case $scope.Status.DECLINED.value:
                case $scope.Status.ACCEPTED.value:
                    notificationsService.sendRequest(shareRideId, bookRideId, 1, status).then(function (results) {
                        if (results.data) {
                            updateStatus(status, shareRideId, bookRideId);
                        }
                    }, function (error) {
                    });
                    break;
            }
        }

        var startTimer = function () {
            var timer = $timeout(function () {
                $timeout.cancel(timer);
                $location.path('/viewshareride');
            }, 2000);
        }
    }]);