﻿'use strict';
app.controller('bookRideController', ['$scope', '$location', '$timeout', 'bookRideService', 'notificationsService', 'ngStatus', function ($scope, $location, $timeout, bookRideService, notificationsService, ngStatus) {

    var addMinutes = function (date, minutes) {
        return new Date(date.getTime() + minutes * 60000);
    };

    var varifyLocations = function (locations) {
        for (var index in locations) {
            var location = locations[index];
            if (typeof (location.name) === 'undefined' || location.name === '' ||
                typeof (location.longitude) === 'undefined' || location.longitude === '' ||
                typeof (location.latitude) === 'undefined' || location.latitude === '') {

                return false;
            }
        }
        return true;
    }

    $scope.savedSuccessfully = false;
    $scope.message = "";
    $scope.bookRideCollection = [];
    $scope.bookRide = {};
    $scope.Status = ngStatus;

    $scope.bookRide.startAt = {
        date: moment(addMinutes(new Date(), 15)).format('YYYY-MM-DD HH:mm'),
        active: false,
        past: false
    };


    if ($location.path() === '/viewbookride') {
        bookRideService.getBookRide().then(function (results) {
            $scope.bookRideCollection = results.data;
            if ($scope.bookRideCollection == null || $scope.bookRideCollection.length == 0) {
                $location.path('/bookride');
            }
        }, function (error) {
            //alert(error.data.message);
        });
    }

    var locations = [];

    var location = {
        "locationType": 1,
        "name": "",
        "longitude": "",
        "latitude": "",
    };

    locations.push(location);

    var location = {
        "locationType": 2,
        "name": "",
        "longitude": "",
        "latitude": "",
    };

    locations.push(location);
    $scope.bookRide.locations = locations;

    $scope.getStatus = function (id) {
        for (var val in $scope.Status) {
            if ($scope.Status[val].value == id) {
                return $scope.Status[val].name;
            }
        }
    };

    $scope.saveBookRide = function () {

        if (varifyLocations($scope.bookRide.locations)) {

            var startAt = moment($scope.bookRide.startAt.date).format();

            bookRideService.saveBookRide($scope.bookRide.locations, startAt).then(function (response) {

                $scope.savedSuccessfully = true;
                $scope.message = "Ride has been added successfully, you will be redicted in 2 seconds.";
                startTimer();

            },
             function (response) {
                 var errors = [];
                 for (var key in response.data.modelState) {
                     for (var i = 0; i < response.data.modelState[key].length; i++) {
                         errors.push(response.data.modelState[key][i]);
                     }
                 }
                 $scope.message = "Failed to register user due to:" + errors.join(' ');
             });
        }
        else {
            $scope.message = "Please select all the locations";
        }
    };

    var updateStatus = function (status, shareRideId, bookRideId) {
        for (var bookRide in $scope.bookRideCollection) {
            if ($scope.bookRideCollection[bookRide].senderId === bookRideId) {
                var rideCollection = $scope.bookRideCollection[bookRide].rideSuggestionEntity;
                for (var rideId in rideCollection) {
                    if (rideCollection[rideId].rideId === shareRideId) {
                        rideCollection[rideId].rideStatus = status;
                    }
                }
            }
        }
    }

    $scope.action = function (status, shareRideId, bookRideId) {
        switch (status) {
            case $scope.Status.SEND.value:
                notificationsService.sendRequest(shareRideId, bookRideId, 2, $scope.Status.SENT.value).then(function (results) {
                    if (results.data) {
                        updateStatus($scope.Status.SENT.value, shareRideId, bookRideId);
                    }
                }, function (error) {
                });
                break;
            case $scope.Status.DECLINED.value:
            case $scope.Status.ACCEPTED.value:
                notificationsService.sendRequest(shareRideId, bookRideId, 2, status).then(function (results) {
                    if (results.data) {
                        updateStatus(status, shareRideId, bookRideId);
                    }
                }, function (error) {
                });
                break;
        }
    }


    var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $location.path('/viewvehicle');
        }, 2000);
    }

}]);