﻿'use strict';
app.controller('indexController', ['$scope', '$location', 'authService', 'signalRService', function ($scope, $location, authService, signalRService) {

    $scope.logOut = function () {
        authService.logOut();
        $location.path('/home');
    }

    $scope.authentication = authService.authentication;
    $scope.notificationCount = 0;

    signalRService.connect($scope.authentication.userName);

    $scope.notes = [];

    $scope.$on('broadcastMessage', function (event, note) {
        $scope.notificationCount = note;
        $scope.$apply();
    });

    $scope.send = function (note) {
        signalRService.send(note);
    };
}]);