﻿'use strict';
app.controller('vehicleController', ['$scope', '$location', '$timeout', 'vehiclesService', function ($scope, $location, $timeout, vehiclesService) {

    $scope.vehicles = [];

    vehiclesService.getVehicles().then(function (results) {

        $scope.vehicles = results.data;

    }, function (error) {
        //alert(error.data.message);
    });

    $scope.makeList = ['Maruti Suzuki', 'MINI', 'Porsche', 'Rolls Royce'];

    $scope.getModel = function () {
        //alert(1);
    }
    //$scope.MakeList = [
    //        { name: 'Maruti Suzuki', code: '1' },
    //        { name: 'MINI', code: '2' },
    //        { name: 'Porsche', code: '3' },
    //        { name: 'Rolls Royce', code: '4' },
    //        { name: 'Tata Motors', code: '5' },
    //        { name: 'Volvo ', code: '6' },
    //        { name: 'Abarth', code: '7' },
    //        { name: 'Bentley', code: '8' },
    //        { name: 'Chevrolet', code: '9' },
    //        { name: 'Ferrari', code: '10' },
    //        { name: 'Ford', code: '11' },
    //        { name: 'Isuzu', code: '12' },
    //        { name: 'Land Rover', code: '13' },
    //        { name: 'Maserati', code: '14' },
    //        { name: 'Mitsubishi', code: '15' },
    //        { name: 'Premier', code: '16' },
    //        { name: 'Skoda', code: '17' },
    //        { name: 'Toyota', code: '18' },
    //        { name: 'Ashok Leyland', code: '19' },
    //        { name: 'BMW ', code: '20' },
    //        { name: 'Datsun', code: '21' },
    //        { name: 'FIAT', code: '22' },
    //        { name: 'Honda', code: '23' }];

    $scope.savedSuccessfully = false;

    $scope.message = "";

    $scope.vehicle = {
        registrationNo: "",
        make: "",
        model: "",
        vehicleType: "",
        color: "",
        capacity: ""
    };

    $scope.redirect = function (val) {
        switch (val) {
            case 'add':
                $location.path('/addvehicle');
                break;
            case 'vehicleHome':
                $location.path('/viewvehicle');
                break;
            default:
                $location.path('/viewvehicle');
                break;
        }
    }

    $scope.saveVehicle = function () {
        vehiclesService.saveVehicle($scope.vehicle).then(function (response) {

            $scope.savedSuccessfully = true;
            $scope.message = "Vehicle has been added successfully, you will be redicted in 2 seconds.";
            startTimer();

        },
         function (response) {
             var errors = [];
             for (var key in response.data.modelState) {
                 for (var i = 0; i < response.data.modelState[key].length; i++) {
                     errors.push(response.data.modelState[key][i]);
                 }
             }
             $scope.message = "Failed to register user due to:" + errors.join(' ');
         });
    };

    var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $location.path('/viewvehicle');
        }, 2000);
    }

}]);