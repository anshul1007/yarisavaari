﻿'use strict';
app.controller('notificationsController', ['$scope', '$location', '$timeout', 'notificationsService', 'ngStatus',
function ($scope, $location, $timeout, notificationsService, ngStatus) {

    $scope.notifications = [];

    $scope.Status = ngStatus;

    notificationsService.getNotifications().then(function (results) {
        $scope.notifications = results.data;
    }, function (error) {
        //alert(error.data.message);
    });

    $scope.action = function (status, senderId, recipientId, rideType) {
        if (rideType === 1) {
            notificationsService.sendRequest(senderId, recipientId, 0, status).then(function (results) {
                if (results.data) {
                    for (var ride in $scope.notifications) {
                        if ($scope.notifications[ride].senderId === recipientId) {
                            var rideCollection = $scope.notifications[ride].rideSuggestionEntity;
                            for (var rideId in rideCollection) {
                                if (rideCollection[rideId].rideId === senderId) {
                                    rideCollection[rideId].rideStatus = status;
                                    return;
                                }
                            }
                        }
                    }
                }
            }, function (error) {
            });
        }
        else {
            notificationsService.sendRequest(recipientId, senderId, 0, status).then(function (results) {
                if (results.data) {
                    for (var ride in $scope.notifications) {
                        if ($scope.notifications[ride].senderId === recipientId) {
                            var rideCollection = $scope.notifications[ride].rideSuggestionEntity;
                            for (var rideId in rideCollection) {
                                if (rideCollection[rideId].rideId === senderId) {
                                    rideCollection[rideId].rideStatus = status;
                                    return;
                                }
                            }
                        }
                    }
                }
            }, function (error) {
            });
        }
    }
}]);