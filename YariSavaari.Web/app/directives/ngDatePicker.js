﻿angular.module("ngDatePicker", [])
    .directive('ngDatePicker', function () {
        return {
            restrict: 'E',
            scope: {
                startDate: "="
            },
            template: '<input type="text" class="form-control" id="pickuptime"  value="startDate" placeholder="Enter your time" aria-describedby="pickuptimeStatus">',
            compile: function (element, attributes) {
                $("#pickuptime").appendDtpicker({
                    "futureOnly": true,
                    "minuteInterval": 5,
                    "closeOnSelected": true
                });
            },
        }
    });
