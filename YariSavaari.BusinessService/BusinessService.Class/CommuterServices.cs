﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using YariSavari.BusinessEntity;
using YariSavari.DataModel;
using YariSavari.DataModel.UnitOfWork;

namespace YariSavari.BusinessService
{
    /// <summary>
    /// Offers services for commuter specific CRUD operations
    /// </summary>
    public class CommuterServices : ICommuterServices
    {
        private readonly UnitOfWork _unitOfWork;

        /// <summary>
        /// Public constructor.
        /// </summary>
        public CommuterServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Fetches commuter details by id
        /// </summary>
        /// <param name="commuterId"></param>
        /// <returns></returns>
        public CommuterEntity GetCommuterById(int commuterId)
        {
            var commuter = _unitOfWork.ShareRideRepository.GetByID(commuterId);
            if (commuter != null)
            {
                Mapper.CreateMap<ShareRide, CommuterEntity>();
                var commuterModel = Mapper.Map<ShareRide, CommuterEntity>(commuter);
                return commuterModel;
            }
            return null;
        }

        /// <summary>
        /// Fetches all the commuters.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CommuterEntity> GetAllCommuters()
        {
            var commuters = _unitOfWork.ShareRideRepository.GetAll().ToList();
            if (commuters.Any())
            {
                Mapper.CreateMap<ShareRide, CommuterEntity>();
                var commutersModel = Mapper.Map<List<ShareRide>, List<CommuterEntity>>(commuters);
                return commutersModel;
            }
            return null;
        }

        /// <summary>
        /// Creates a commuter
        /// </summary>
        /// <param name="commuterEntity"></param>
        /// <returns></returns>
        public Guid CreateCommuter(CommuterEntity commuterEntity)
        {
            using (var scope = new TransactionScope())
            {
                var commuter = new ShareRide
                {
                    //UserId = commuterEntity.UserId,
                    //Path = Mapper.Map<PathEntity, Path>(commuterEntity.Path),
                    //StartAt = commuterEntity.StartAt,
                    //IsActive = commuterEntity.IsActive,
                    //SendRequestToAll = commuterEntity.SendRequestToAll,
                    //Status = commuterEntity.Status
                };
                _unitOfWork.ShareRideRepository.Insert(commuter);
                _unitOfWork.Save();
                scope.Complete();
                return commuter.ShareRideId;
            }
        }

        /// <summary>
        /// Updates a commuter
        /// </summary>
        /// <param name="commuterId"></param>
        /// <param name="commuterEntity"></param>
        /// <returns></returns>
        public bool UpdateCommuter(int commuterId, CommuterEntity commuterEntity)
        {
            var success = false;
            if (commuterEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var commuter = _unitOfWork.ShareRideRepository.GetByID(commuterId);
                    if (commuter != null)
                    {
                        //commuter.UserId = commuterEntity.UserId;
                        //commuter.Path = Mapper.Map<PathEntity, Path>(commuterEntity.Path);
                        //commuter.StartAt = commuterEntity.StartAt;
                        //commuter.IsActive = commuterEntity.IsActive;
                        //commuter.SendRequestToAll = commuterEntity.SendRequestToAll;
                        //commuter.Status = commuterEntity.Status;
                        _unitOfWork.ShareRideRepository.Update(commuter);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        /// <summary>
        /// Deletes a particular commuter
        /// </summary>
        /// <param name="commuterId"></param>
        /// <returns></returns>
        public bool DeleteCommuter(int commuterId)
        {
            var success = false;
            if (commuterId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var commuter = _unitOfWork.ShareRideRepository.GetByID(commuterId);
                    if (commuter != null)
                    {

                        _unitOfWork.ShareRideRepository.Delete(commuter);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}