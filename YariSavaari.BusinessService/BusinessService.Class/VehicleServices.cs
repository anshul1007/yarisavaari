﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using YariSavaari.BusinessEntity;
using YariSavaari.DataModel;
using YariSavaari.DataModel.UnitOfWork;

namespace YariSavaari.BusinessService
{
    /// <summary>
    /// Offers services for vehicle specific CRUD operations
    /// </summary>
    public class VehicleServices : IVehicleServices
    {
        private readonly UnitOfWork _unitOfWork;

        /// <summary>
        /// Public constructor.
        /// </summary>
        //public VehicleServices(UnitOfWork unitOfWork)
        //{
        //    _unitOfWork = unitOfWork;
        //}

        public VehicleServices()
        {
            _unitOfWork = new UnitOfWork();
        }

        /// <summary>
        /// Fetches vehicle details by id
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        public VehicleEntity GetVehicleById(int vehicleId)
        {
            var vehicle = _unitOfWork.VehicleRepository.GetByID(vehicleId);
            if (vehicle != null)
            {
                Mapper.CreateMap<Vehicle, VehicleEntity>();
                var vehicleModel = Mapper.Map<Vehicle, VehicleEntity>(vehicle);
                return vehicleModel;
            }
            return null;
        }

        public VehicleEntity GetVehicleById(int vehicleId, string userId)
        {
            var vehicle = _unitOfWork.VehicleRepository.Get(v => v.VehicleId == vehicleId && v.UserId == userId);
            if (vehicle != null)
            {
                Mapper.CreateMap<Vehicle, VehicleEntity>();
                var vehicleModel = Mapper.Map<Vehicle, VehicleEntity>(vehicle);
                return vehicleModel;
            }
            return null;
        }

        /// <summary>
        /// Fetches all the vehicles.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VehicleEntity> GetAllVehicles()
        {
            var vehicles = _unitOfWork.VehicleRepository.GetAll().ToList();
            if (vehicles.Any())
            {
                Mapper.CreateMap<Vehicle, VehicleEntity>();
                var vehiclesModel = Mapper.Map<List<Vehicle>, List<VehicleEntity>>(vehicles);
                return vehiclesModel;
            }
            return null;
        }

        public IEnumerable<VehicleEntity> GetAllVehicles(string userId)
        {
            var vehicles = _unitOfWork.VehicleRepository.GetMany(v => v.UserId == userId).ToList();
            if (vehicles.Any())
            {
                Mapper.CreateMap<Vehicle, VehicleEntity>();
                var vehiclesModel = Mapper.Map<List<Vehicle>, List<VehicleEntity>>(vehicles);
                return vehiclesModel;
            }
            return null;
        }
        /// <summary>
        /// Creates a vehicle
        /// </summary>
        /// <param name="vehicleEntity"></param>
        /// <returns></returns>
        public int CreateVehicle(VehicleEntity vehicleEntity)
        {
            using (var scope = new TransactionScope())
            {
                var isExist = _unitOfWork.VehicleRepository.Get(v => v.RegistrationNo == vehicleEntity.RegistrationNo && v.UserId == vehicleEntity.UserId);

                if (isExist == null)
                {
                    var vehicle = new Vehicle
                    {
                        UserId = vehicleEntity.UserId,
                        RegistrationNo = vehicleEntity.RegistrationNo,
                        VehicleType = vehicleEntity.VehicleType,
                        Make = vehicleEntity.Make,
                        Model = vehicleEntity.Model,
                        Color = vehicleEntity.Color,
                        Capacity = vehicleEntity.Capacity,
                        OnlyFemaleCommuter = vehicleEntity.OnlyFemaleCommuter
                    };
                    _unitOfWork.VehicleRepository.Insert(vehicle);
                    _unitOfWork.Save();
                    scope.Complete();
                    return vehicle.VehicleId;
                }
                else
                {
                    throw new Exception("Already added the same registration no");
                }
            }
        }

        /// <summary>
        /// Updates a vehicle
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="vehicleEntity"></param>
        /// <returns></returns>
        public bool UpdateVehicle(int vehicleId, VehicleEntity vehicleEntity, string userId)
        {
            var success = false;
            if (vehicleEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    Mapper.CreateMap<ApplicationUser, ApplicationUserEntity>();
                    var vehicle = _unitOfWork.VehicleRepository.GetByID(vehicleId);
                    if (vehicle != null && vehicle.UserId == userId)
                    {
                        vehicle.RegistrationNo = vehicleEntity.RegistrationNo;
                        vehicle.VehicleType = vehicleEntity.VehicleType;
                        vehicle.Make = vehicleEntity.Make;
                        vehicle.Model = vehicleEntity.Model;
                        vehicle.Color = vehicleEntity.Color;
                        vehicle.Capacity = vehicleEntity.Capacity;
                        vehicle.OnlyFemaleCommuter = vehicleEntity.OnlyFemaleCommuter;
                        _unitOfWork.VehicleRepository.Update(vehicle);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        /// <summary>
        /// Deletes a particular vehicle
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        public bool DeleteVehicle(int vehicleId)
        {
            throw new NotImplementedException();
            //var success = false;
            //if (vehicleId > 0)
            //{
            //    using (var scope = new TransactionScope())
            //    {
            //        var vehicle = _unitOfWork.VehicleRepository.GetByID(vehicleId);
            //        if (vehicle != null)
            //        {

            //            _unitOfWork.VehicleRepository.Delete(vehicle);
            //            _unitOfWork.Save();
            //            scope.Complete();
            //            success = true;
            //        }
            //    }
            //}
            //return success;
        }
    }
}