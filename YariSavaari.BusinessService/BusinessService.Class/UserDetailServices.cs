﻿//using AutoMapper;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Transactions;
//using YariSavari.BusinessEntity;
//using YariSavari.DataModel;
//using YariSavari.DataModel.UnitOfWork;

//namespace YariSavari.BusinessService
//{
//    /// <summary>
//    /// Offers services for user specific CRUD operations
//    /// </summary>
//    public class UserDetailServices : IUserDetailServices
//    {
//        private readonly UnitOfWork _unitOfWork;

//        /// <summary>
//        /// Public constructor.
//        /// </summary>
//        public UserDetailServices(UnitOfWork unitOfWork)
//        {
//            _unitOfWork = unitOfWork;
//        }

//        /// <summary>
//        /// Fetches user details by id
//        /// </summary>
//        /// <param name="userId"></param>
//        /// <returns></returns>
//        public UserDetailEntity GetUserDetailById(int userId)
//        {
//            var user = _unitOfWork.UserDetailRepository.GetByID(userId);
//            if (user != null)
//            {
//                Mapper.CreateMap<UserDetail, UserDetailEntity>();
//                var userModel = Mapper.Map<UserDetail, UserDetailEntity>(user);
//                return userModel;
//            }
//            return null;
//        }

//        /// <summary>
//        /// Fetches all the users.
//        /// </summary>
//        /// <returns></returns>
//        public IEnumerable<UserDetailEntity> GetAllUsersDetail()
//        {
//            var users = _unitOfWork.UserDetailRepository.GetAll().ToList();
//            if (users.Any())
//            {
//                Mapper.CreateMap<UserDetail, UserDetailEntity>();
//                var usersModel = Mapper.Map<List<UserDetail>, List<UserDetailEntity>>(users);
//                return usersModel;
//            }
//            return null;
//        }

//        /// <summary>
//        /// Creates a user
//        /// </summary>
//        /// <param name="userEntity"></param>
//        /// <returns></returns>
//        public string CreateUserDetail(UserDetailEntity userEntity)
//        {
//            using (var scope = new TransactionScope())
//            {
//                Mapper.CreateMap<VehicleEntity, Vehicle>();
//                Mapper.CreateMap<PathEntity, Path>();
//                Mapper.CreateMap<LocationEntity, Location>();
//                var user = new UserDetail
//                {
//                    Gender = userEntity.Gender,
//                    TimeFlexibility = userEntity.TimeFlexibility,
//                    PickAreaRange = userEntity.PickAreaRange,
//                    DestinationAreaRange = userEntity.DestinationAreaRange,
//                    Car = Mapper.Map<IList<VehicleEntity>, IList<Vehicle>>(userEntity.Car),
//                    Path = Mapper.Map<IList<PathEntity>, IList<Path>>(userEntity.Path)
//                };
//                _unitOfWork.UserDetailRepository.Insert(user);
//                _unitOfWork.Save();
//                scope.Complete();
//                return user.ApplicationUser.Id;
//            }
//        }

//        /// <summary>
//        /// Updates a user
//        /// </summary>
//        /// <param name="userId"></param>
//        /// <param name="userEntity"></param>
//        /// <returns></returns>
//        public bool UpdateUserDetail(int userId, UserDetailEntity userEntity)
//        {
//            var success = false;
//            if (userEntity != null)
//            {
//                using (var scope = new TransactionScope())
//                {
//                    var user = _unitOfWork.UserDetailRepository.GetByID(userId);
//                    if (user != null)
//                    {
//                        //user.Name = userEntity.Name;
//                        //user.Email = userEntity.Email;
//                        //user.Mobile = userEntity.Mobile;
//                        //user.Password = userEntity.Password;
//                        user.Gender = userEntity.Gender;
//                        user.TimeFlexibility = userEntity.TimeFlexibility;
//                        user.PickAreaRange = userEntity.PickAreaRange;
//                        user.DestinationAreaRange = userEntity.DestinationAreaRange;
//                        user.Car= Mapper.Map<IList<VehicleEntity>, IList<Vehicle>>(userEntity.Car);
//                        user.Path = Mapper.Map<IList<PathEntity>, IList<Path>>(userEntity.Path);
//                        _unitOfWork.UserDetailRepository.Update(user);
//                        _unitOfWork.Save();
//                        scope.Complete();
//                        success = true;
//                    }
//                }
//            }
//            return success;
//        }

//        /// <summary>
//        /// Deletes a particular user
//        /// </summary>
//        /// <param name="userId"></param>
//        /// <returns></returns>
//        public bool DeleteUserDetail(int userId)
//        {
//            var success = false;
//            if (userId > 0)
//            {
//                using (var scope = new TransactionScope())
//                {
//                    var user = _unitOfWork.UserDetailRepository.GetByID(userId);
//                    if (user != null)
//                    {

//                        _unitOfWork.UserDetailRepository.Delete(user);
//                        _unitOfWork.Save();
//                        scope.Complete();
//                        success = true;
//                    }
//                }
//            }
//            return success;
//        }
//    }
//}