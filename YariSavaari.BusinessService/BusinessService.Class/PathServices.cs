﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using YariSavari.BusinessEntity;
using YariSavari.DataModel;
using YariSavari.DataModel.UnitOfWork;

namespace YariSavari.BusinessService
{
    /// <summary>
    /// Offers services for path specific CRUD operations
    /// </summary>
    public class PathServices : IPathServices
    {
        private readonly UnitOfWork _unitOfWork;

        /// <summary>
        /// Public constructor.
        /// </summary>
        public PathServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Fetches path details by id
        /// </summary>
        /// <param name="pathId"></param>
        /// <returns></returns>
        public PathEntity GetPathById(int pathId)
        {
            var path = _unitOfWork.PathRepository.GetByID(pathId);
            if (path != null)
            {
                Mapper.CreateMap<Path, PathEntity>();
                var pathModel = Mapper.Map<Path, PathEntity>(path);
                return pathModel;
            }
            return null;
        }

        /// <summary>
        /// Fetches all the paths.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PathEntity> GetAllPaths()
        {
            var paths = _unitOfWork.PathRepository.GetAll().ToList();
            if (paths.Any())
            {
                Mapper.CreateMap<Path, PathEntity>();
                var pathsModel = Mapper.Map<List<Path>, List<PathEntity>>(paths);
                return pathsModel;
            }
            return null;
        }

        /// <summary>
        /// Creates a path
        /// </summary>
        /// <param name="pathEntity"></param>
        /// <returns></returns>
        public int CreatePath(PathEntity pathEntity)
        {
            using (var scope = new TransactionScope())
            {
                var path = new Path
                {
                    //Location = Mapper.Map<IList<LocationEntity>, IList<Location>>(pathEntity.Location),
                    //LastUsedAt = pathEntity.LastUsedAt
                };
                _unitOfWork.PathRepository.Insert(path);
                _unitOfWork.Save();
                scope.Complete();
                return path.PathId;
            }
        }

        /// <summary>
        /// Updates a path
        /// </summary>
        /// <param name="pathId"></param>
        /// <param name="pathEntity"></param>
        /// <returns></returns>
        public bool UpdatePath(int pathId, PathEntity pathEntity)
        {
            var success = false;
            if (pathEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var path = _unitOfWork.PathRepository.GetByID(pathId);
                    if (path != null)
                    {
                        //path.Location = Mapper.Map<IList<LocationEntity>, IList<Location>>(pathEntity.Location);
                        //path.LastUsedAt = pathEntity.LastUsedAt;
                        //_unitOfWork.PathRepository.Update(path);
                        //_unitOfWork.Save();
                        //scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        /// <summary>
        /// Deletes a particular path
        /// </summary>
        /// <param name="pathId"></param>
        /// <returns></returns>
        public bool DeletePath(int pathId)
        {
            var success = false;
            if (pathId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var path = _unitOfWork.PathRepository.GetByID(pathId);
                    if (path != null)
                    {

                        _unitOfWork.PathRepository.Delete(path);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}