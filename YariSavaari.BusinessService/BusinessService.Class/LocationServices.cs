﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using YariSavari.BusinessEntity;
using YariSavari.DataModel;
using YariSavari.DataModel.UnitOfWork;

namespace YariSavari.BusinessService
{
    /// <summary>
    /// Offers services for location specific CRUD operations
    /// </summary>
    public class LocationServices : ILocationServices
    {
        private readonly UnitOfWork _unitOfWork;

        /// <summary>
        /// Public constructor.
        /// </summary>
        public LocationServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Fetches location details by id
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        public LocationEntity GetLocationById(int locationId)
        {
            var location = _unitOfWork.LocationRepository.GetByID(locationId);
            if (location != null)
            {
                Mapper.CreateMap<Location, LocationEntity>();
                var locationModel = Mapper.Map<Location, LocationEntity>(location);
                return locationModel;
            }
            return null;
        }

        /// <summary>
        /// Fetches all the locations.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<LocationEntity> GetAllLocations()
        {
            var locations = _unitOfWork.LocationRepository.GetAll().ToList();
            if (locations.Any())
            {
                Mapper.CreateMap<Location, LocationEntity>();
                var locationsModel = Mapper.Map<List<Location>, List<LocationEntity>>(locations);
                return locationsModel;
            }
            return null;
        }

        /// <summary>
        /// Creates a location
        /// </summary>
        /// <param name="locationEntity"></param>
        /// <returns></returns>
        public int CreateLocation(LocationEntity locationEntity)
        {
            using (var scope = new TransactionScope())
            {
                var location = new Location
                {
                    Name = locationEntity.Name,
                    LocationType = (YariSavari.DataModel.LocationType)locationEntity.LocationType,
                    Longitude = locationEntity.Longitude,
                    Latitude = locationEntity.Latitude
                };
                _unitOfWork.LocationRepository.Insert(location);
                _unitOfWork.Save();
                scope.Complete();
                return location.LocationId;
            }
        }

        /// <summary>
        /// Updates a location
        /// </summary>
        /// <param name="locationId"></param>
        /// <param name="locationEntity"></param>
        /// <returns></returns>
        public bool UpdateLocation(int locationId, LocationEntity locationEntity)
        {
            var success = false;
            if (locationEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var location = _unitOfWork.LocationRepository.GetByID(locationId);
                    if (location != null)
                    {
                        location.LocationType = (YariSavari.DataModel.LocationType)locationEntity.LocationType;
                        location.Name = locationEntity.Name;
                        location.Longitude = locationEntity.Longitude;
                        location.Latitude = locationEntity.Latitude;
                        _unitOfWork.LocationRepository.Update(location);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        /// <summary>
        /// Deletes a particular location
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        public bool DeleteLocation(int locationId)
        {
            var success = false;
            if (locationId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var location = _unitOfWork.LocationRepository.GetByID(locationId);
                    if (location != null)
                    {

                        _unitOfWork.LocationRepository.Delete(location);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}