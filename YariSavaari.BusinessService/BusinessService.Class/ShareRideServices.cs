﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using YariSavaari.BusinessEntity;
using YariSavaari.DataModel;
using YariSavaari.DataModel.UnitOfWork;
using YariSavaari.Utility;

namespace YariSavaari.BusinessService
{
    /// <summary>
    /// Offers services for ShareRide specific CRUD operations
    /// </summary>
    public class ShareRideServices : IShareRideServices
    {
        private readonly UnitOfWork _unitOfWork;

        public ShareRideServices()
        {
            _unitOfWork = new UnitOfWork();
        }

        /// <summary>
        /// Fetches all the ShareRides.
        /// </summary>
        /// <returns></returns>
        //public IEnumerable<ShareRideEntity> GetAllShareRides()
        //{
        //    var shareRide = _unitOfWork.ShareRideRepository.GetWithInclude(s => s.IsActive == true, new string[] { "User", "Vehicle" }).ToList();
        //    if (shareRide.Any())
        //    {
        //        Mapper.CreateMap<ShareRide, ShareRideEntity>();
        //        Mapper.CreateMap<ApplicationUser, ApplicationUserEntity>();
        //        Mapper.CreateMap<Vehicle, VehicleEntity>();
        //        var shareRideModel = Mapper.Map<List<ShareRide>, List<ShareRideEntity>>(shareRide);
        //        return shareRideModel;
        //    }
        //    return null;
        //}

        public string CreateShareRide(List<PathEntity> path, DateTime startAt, int vehicleId, string userId)
        {
            using (var scope = new TransactionScope())
            {
                var vehicle = _unitOfWork.VehicleRepository.GetFirst(x => x.VehicleId == vehicleId);
                var user = _unitOfWork.ApplicationUserRepository.GetFirst(x => x.Id == userId);

                var isExist = (vehicle.UserId == userId);

                if (isExist)
                {
                    Mapper.CreateMap<LocationEntity, Location>();
                    Mapper.CreateMap<PathEntity, Path>();

                    var shareRide = new ShareRide
                    {
                        ShareRideId = Guid.NewGuid(),
                        User = user,
                        Vehicle = vehicle,
                        StartAt = startAt,
                        Path = Mapper.Map<List<PathEntity>, List<Path>>(path),
                        IsActive = true
                    };
                    _unitOfWork.ShareRideRepository.Insert(shareRide);
                    _unitOfWork.Save();
                    scope.Complete();
                    return shareRide.ShareRideId.ToString();
                }
                else
                {
                    throw new Exception("Vehicle does not belongs to you");
                }
            }
        }

        public List<PickedPassengerEntity> PickPassenger(string userId)
        {

            Mapper.CreateMap<PickedPassenger, PickedPassengerEntity>();
            Mapper.CreateMap<RideSuggestion, RideSuggestionEntity>();

            var result = new List<PickedPassengerEntity>();

            var shareRideIds = _unitOfWork.ShareRideRepository
                .GetWithInclude(s => s.User.Id == userId && s.StartAt >= DateTime.Now && s.IsActive == true, new string[] { "User" })
                .Select(x => new
                {
                    Id = x.ShareRideId,
                    Path = x.Path.Where(y => y.From.LocationType == LocationType.Source
                            && y.To.LocationType == LocationType.Destination).Select(z => new { From = z.From, To = z.To }),
                    StartAt = x.StartAt
                }).ToList();

            foreach (var ride in shareRideIds)
            {
                using (var context = new YariSavaariDBContext())
                {
                    var sqlParameter = new SqlParameter()
                    {
                        ParameterName = "@ShareRideId",
                        SqlDbType = SqlDbType.UniqueIdentifier,
                        Size = 50,
                        Value = ride.Id
                    };

                    var bookRide = context.Database
                        .SqlQuery<RideSuggestion>("usp_PickPassengersForShareRide @ShareRideId", sqlParameter)
                        .ToList();

                    if (bookRide != null)
                    {
                        var bookRideModel = Mapper.Map<List<RideSuggestion>, List<RideSuggestionEntity>>(bookRide);
                        result.Add(new PickedPassengerEntity()
                        {
                            SenderId = ride.Id,
                            StartAt = ride.StartAt,
                            FromName = ride.Path.FirstOrDefault().From.Name,
                            ToName = ride.Path.FirstOrDefault().To.Name,
                            RideSuggestionEntity = bookRideModel
                        });
                    }
                }
            }

            return result;
        }
    }
}