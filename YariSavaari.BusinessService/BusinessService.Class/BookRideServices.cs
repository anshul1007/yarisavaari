﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using YariSavaari.BusinessEntity;
using YariSavaari.DataModel;
using YariSavaari.DataModel.UnitOfWork;
using YariSavaari.Utility;

namespace YariSavaari.BusinessService
{
    /// <summary>
    /// Offers services for BookRide specific CRUD operations
    /// </summary>
    public class BookRideServices : IBookRideServices
    {
        private readonly UnitOfWork _unitOfWork;

        public BookRideServices()
        {
            _unitOfWork = new UnitOfWork();
        }

        /// <summary>
        /// Fetches all the BookRides.
        /// </summary>
        /// <returns></returns>
        //public IEnumerable<BookRideEntity> GetAllBookRides()
        //{
        //    var bookRide = _unitOfWork.BookRideRepository.GetWithInclude(s => s.IsActive == true, new string[] { "User" }).ToList();
        //    if (bookRide.Any())
        //    {
        //        Mapper.CreateMap<BookRide, BookRideEntity>();
        //        Mapper.CreateMap<ApplicationUser, ApplicationUserEntity>();
        //        var bookRideModel = Mapper.Map<List<BookRide>, List<BookRideEntity>>(bookRide);
        //        return bookRideModel;
        //    }
        //    return null;
        //}

        public string CreateBookRide(List<PathEntity> path, DateTime startAt, string userId)
        {
            using (var scope = new TransactionScope())
            {
                var user = _unitOfWork.ApplicationUserRepository.GetFirst(x => x.Id == userId);

                Mapper.CreateMap<LocationEntity, Location>();
                Mapper.CreateMap<PathEntity, Path>();

                var bookRide = new BookRide
                {
                    BookRideId = Guid.NewGuid(),
                    User = user,
                    StartAt = startAt,
                    Path = Mapper.Map<List<PathEntity>, List<Path>>(path),
                    IsActive = true
                };
                _unitOfWork.BookRideRepository.Insert(bookRide);
                _unitOfWork.Save();
                scope.Complete();
                return bookRide.BookRideId.ToString();
            }
        }

        public List<PickedPassengerEntity> PickPassenger(string userId)
        {

            Mapper.CreateMap<PickedPassenger, PickedPassengerEntity>();
            Mapper.CreateMap<RideSuggestion, RideSuggestionEntity>();

            var result = new List<PickedPassengerEntity>();

            var bookRideIds = _unitOfWork.BookRideRepository
                .GetWithInclude(s => s.User.Id == userId && s.StartAt >= DateTime.Now && s.IsActive == true, new string[] { "User" })
                .Select(x => new
                {
                    Id = x.BookRideId,
                    Path = x.Path.Where(y => y.From.LocationType == LocationType.Source
                            && y.To.LocationType == LocationType.Destination).Select(z => new { From = z.From, To = z.To }),
                    StartAt = x.StartAt
                }).ToList();

            foreach (var ride in bookRideIds)
            {
                using (var context = new YariSavaariDBContext())
                {
                    var sqlParameter = new SqlParameter()
                    {
                        ParameterName = "@BookRideId",
                        SqlDbType = SqlDbType.UniqueIdentifier,
                        Size = 50,
                        Value = ride.Id
                    };

                    var bookRide = context.Database
                        .SqlQuery<RideSuggestion>("usp_PickPassengersForBookRide @BookRideId", sqlParameter)
                        .ToList();

                    if (bookRide != null)
                    {
                        var shareRideModel = Mapper.Map<List<RideSuggestion>, List<RideSuggestionEntity>>(bookRide);
                        result.Add(new PickedPassengerEntity()
                        {
                            SenderId = ride.Id,
                            StartAt = ride.StartAt,
                            FromName = ride.Path.FirstOrDefault().From.Name,
                            ToName = ride.Path.FirstOrDefault().To.Name,
                            RideSuggestionEntity = shareRideModel
                        });
                    }
                }
            }

            return result;
        }
    }
}