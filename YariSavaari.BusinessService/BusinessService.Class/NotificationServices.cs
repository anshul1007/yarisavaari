﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using YariSavaari.BusinessEntity;
using YariSavaari.DataModel;
using YariSavaari.DataModel.UnitOfWork;
using YariSavaari.Utility;

namespace YariSavaari.BusinessService
{
    /// <summary>
    /// Offers services for Notification specific CRUD operations
    /// </summary>
    public class NotificationServices : INotificationServices
    {
        private readonly UnitOfWork _unitOfWork;

        public NotificationServices()
        {
            _unitOfWork = new UnitOfWork();
        }

        /// <summary>
        /// Fetches all the Notifications.
        /// </summary>
        /// <returns></returns>
        public List<PickedPassengerEntity> GetAllNotifications(string userId)
        {
            Mapper.CreateMap<PickedPassenger, PickedPassengerEntity>();
            Mapper.CreateMap<RideSuggestion, RideSuggestionEntity>();

            var result = new List<PickedPassengerEntity>();

            var rideIds = _unitOfWork.BookRideRepository
                            .GetWithInclude(s => s.User.Id == userId && s.StartAt >= DateTime.Now && s.IsActive == true, new string[] { "User" })
                            .Select(x => new
                            {
                                Id = x.BookRideId,
                                Path = x.Path.Where(y => y.From.LocationType == LocationType.Source
                                        && y.To.LocationType == LocationType.Destination).Select(z => new { From = z.From, To = z.To }),
                                StartAt = x.StartAt
                            }).ToList();

            foreach (var ride in rideIds)
            {
                using (var context = new YariSavaariDBContext())
                {
                    var sqlParameter = new SqlParameter()
                    {
                        ParameterName = "@BookRideId",
                        SqlDbType = System.Data.SqlDbType.UniqueIdentifier,
                        Value = ride.Id
                    };

                    var shareRide = context.Database
                        .SqlQuery<RideSuggestion>("usp_BookRideNotifications @BookRideId", sqlParameter)
                        .ToList();

                    if (shareRide != null)
                    {
                        var shareRideModel = Mapper.Map<List<RideSuggestion>, List<RideSuggestionEntity>>(shareRide);
                        result.Add(new PickedPassengerEntity()
                        {
                            SenderId = ride.Id,
                            StartAt = ride.StartAt,
                            FromName = ride.Path.FirstOrDefault().From.Name,
                            ToName = ride.Path.FirstOrDefault().To.Name,
                            RideType = RideType.Share,
                            RideSuggestionEntity = shareRideModel
                        });
                    }
                }
            }

            rideIds = _unitOfWork.ShareRideRepository
                            .GetWithInclude(s => s.User.Id == userId && s.StartAt >= DateTime.Now && s.IsActive == true, new string[] { "User" })
                            .Select(x => new
                            {
                                Id = x.ShareRideId,
                                Path = x.Path.Where(y => y.From.LocationType == LocationType.Source
                                        && y.To.LocationType == LocationType.Destination).Select(z => new { From = z.From, To = z.To }),
                                StartAt = x.StartAt
                            }).ToList();
            foreach (var ride in rideIds)
            {
                using (var context = new YariSavaariDBContext())
                {
                    var sqlParameter = new SqlParameter()
                    {
                        ParameterName = "@ShareRideId",
                        SqlDbType = System.Data.SqlDbType.UniqueIdentifier,
                        Value = ride.Id
                    };

                    var shareRide = context.Database
                        .SqlQuery<RideSuggestion>("usp_ShareRideNotifications @ShareRideId", sqlParameter)
                        .ToList();

                    if (shareRide != null)
                    {
                        var shareRideModel = Mapper.Map<List<RideSuggestion>, List<RideSuggestionEntity>>(shareRide);
                        result.Add(new PickedPassengerEntity()
                        {
                            SenderId = ride.Id,
                            StartAt = ride.StartAt,
                            FromName = ride.Path.FirstOrDefault().From.Name,
                            ToName = ride.Path.FirstOrDefault().To.Name,
                            RideType = RideType.Book,
                            RideSuggestionEntity = shareRideModel
                        });
                    }
                }
            }
            return result;
        }

        public bool ShareRequest(string userId, CoRideDetailsEntity rideRequest)
        {
            using (var scope = new TransactionScope())
            {
                var isExist = _unitOfWork.CoRideDetailsRepository.Get(x => x.ShareRideId == rideRequest.ShareRideId
                    && x.BookRideId == rideRequest.BookRideId);
                if (isExist == null)
                {
                    if (rideRequest.RideStatus == RideStatus.Sent)
                    {
                        var coRideDetails = new CoRideDetails
                                           {
                                               ShareRideId = rideRequest.ShareRideId,
                                               BookRideId = rideRequest.BookRideId,
                                               RideStatus = rideRequest.RideStatus,
                                               RideType = rideRequest.RideType,
                                           };
                        _unitOfWork.CoRideDetailsRepository.Insert(coRideDetails);
                        _unitOfWork.Save();
                        scope.Complete();
                        return (coRideDetails.Id > 0);
                    }
                }
                else
                {
                    isExist.RideStatus = rideRequest.RideStatus;
                    _unitOfWork.CoRideDetailsRepository.Update(isExist);
                    _unitOfWork.Save();
                    scope.Complete();
                    return true;
                }
            }
            return false;
        }

    }
}