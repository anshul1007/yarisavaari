﻿//using AutoMapper;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Transactions;
//using YariSavari.BusinessEntity;
//using YariSavari.DataModel;
//using YariSavari.DataModel.UnitOfWork;

//namespace YariSavari.BusinessService
//{
//    /// <summary>
//    /// Offers services for ride specific CRUD operations
//    /// </summary>
//    public class RideServices : IRideServices
//    {
//        private readonly UnitOfWork _unitOfWork;

//        /// <summary>
//        /// Public constructor.
//        /// </summary>
//        public RideServices(UnitOfWork unitOfWork)
//        {
//            _unitOfWork = unitOfWork;
//        }

//        /// <summary>
//        /// Fetches ride details by id
//        /// </summary>
//        /// <param name="rideId"></param>
//        /// <returns></returns>
//        public RideEntity GetRideById(int rideId)
//        {
//            var ride = _unitOfWork.BookRideRepository.GetByID(rideId);
//            if (ride != null)
//            {
//                Mapper.CreateMap<BookRide, RideEntity>();
//                var rideModel = Mapper.Map<BookRide, RideEntity>(ride);
//                return rideModel;
//            }
//            return null;
//        }

//        /// <summary>
//        /// Fetches all the rides.
//        /// </summary>
//        /// <returns></returns>
//        public IEnumerable<RideEntity> GetAllRides()
//        {
//            var rides = _unitOfWork.BookRideRepository.GetAll().ToList();
//            if (rides.Any())
//            {
//                Mapper.CreateMap<BookRide, RideEntity>();
//                var ridesModel = Mapper.Map<List<BookRide>, List<RideEntity>>(rides);
//                return ridesModel;
//            }
//            return null;
//        }

//        /// <summary>
//        /// Creates a ride
//        /// </summary>
//        /// <param name="rideEntity"></param>
//        /// <returns></returns>
//        public Guid CreateRide(RideEntity rideEntity)
//        {
//            using (var scope = new TransactionScope())
//            {
//                var ride = new BookRide
//                {
//                    //Path = Mapper.Map<PathEntity, Path>(rideEntity.Path),
//                    //CarId = rideEntity.CarId,
//                    //Occupied = rideEntity.Occupied,
//                    //IsActive = rideEntity.IsActive,
//                    //StartAt = rideEntity.StartAt,
//                    //Commuter = Mapper.Map<IList<CommuterEntity>, IList<ShareRide>>(rideEntity.Commuter)
//                };
//                _unitOfWork.BookRideRepository.Insert(ride);
//                _unitOfWork.Save();
//                scope.Complete();
//                return ride.BookRideId;
//            }
//        }

//        /// <summary>
//        /// Updates a ride
//        /// </summary>
//        /// <param name="rideId"></param>
//        /// <param name="rideEntity"></param>
//        /// <returns></returns>
//        public bool UpdateRide(int rideId, RideEntity rideEntity)
//        {
//            var success = false;
//            if (rideEntity != null)
//            {
//                using (var scope = new TransactionScope())
//                {
//                    var ride = _unitOfWork.BookRideRepository.GetByID(rideId);
//                    if (ride != null)
//                    {
//                        //ride.Path = Mapper.Map<PathEntity, Path>(rideEntity.Path);
//                        //ride.CarId = rideEntity.CarId;
//                        //ride.Occupied = rideEntity.Occupied;
//                        //ride.IsActive = rideEntity.IsActive;
//                        //ride.StartAt = rideEntity.StartAt;
//                        //ride.Commuter = Mapper.Map<IList<CommuterEntity>, IList<ShareRide>>(rideEntity.Commuter);
//                        //_unitOfWork.BookRideRepository.Update(ride);
//                        //_unitOfWork.Save();
//                        //scope.Complete();
//                        //success = true;
//                    }
//                }
//            }
//            return success;
//        }

//        /// <summary>
//        /// Deletes a particular ride
//        /// </summary>
//        /// <param name="rideId"></param>
//        /// <returns></returns>
//        public bool DeleteRide(int rideId)
//        {
//            var success = false;
//            if (rideId > 0)
//            {
//                using (var scope = new TransactionScope())
//                {
//                    var ride = _unitOfWork.BookRideRepository.GetByID(rideId);
//                    if (ride != null)
//                    {

//                        _unitOfWork.BookRideRepository.Delete(ride);
//                        _unitOfWork.Save();
//                        scope.Complete();
//                        success = true;
//                    }
//                }
//            }
//            return success;
//        }
//    }
//}