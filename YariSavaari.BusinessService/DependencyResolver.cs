﻿using System.ComponentModel.Composition;
using YariSavaari.DataModel;
using YariSavaari.DataModel.UnitOfWork;
using YariSavaari.Resolver;
using YariSavaari.BusinessService;

namespace YariSavaari.BusinessService
{
    [Export(typeof(IComponent))]
    public class DependencyResolver : IComponent
    {
        public void SetUp(IRegisterComponent registerComponent)
        {
            registerComponent.RegisterType<IBookRideServices, BookRideServices>();
            registerComponent.RegisterType<IShareRideServices, ShareRideServices>();
            registerComponent.RegisterType<IVehicleServices, VehicleServices>();
        }
    }
}