﻿using System;
using System.Collections.Generic;
using YariSavaari.BusinessEntity;

namespace YariSavaari.BusinessService
{
    public interface IBookRideServices
    {
        //IEnumerable<BookRideEntity> GetAllBookRides();
        string CreateBookRide(List<PathEntity> bookRideEntity, DateTime startAt, string userId);
        List<PickedPassengerEntity> PickPassenger(string userId);
        //bool ShareRequest(string userId, CoRideDetailsEntity rideRequest);
    }
}
