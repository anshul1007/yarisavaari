﻿using YariSavaari.BusinessEntity;
using System.Collections.Generic;

namespace YariSavaari.BusinessService
{
    public interface IVehicleServices
    {
        VehicleEntity GetVehicleById(int vehicleId);
        VehicleEntity GetVehicleById(int vehicleId, string userId);
        IEnumerable<VehicleEntity> GetAllVehicles();
        IEnumerable<VehicleEntity> GetAllVehicles(string userId);
        int CreateVehicle(VehicleEntity vehicleEntity);
        bool UpdateVehicle(int vehicleId, VehicleEntity vehicleEntity, string userId);
        bool DeleteVehicle(int vehicleId);
    }
}
