﻿using YariSavari.BusinessEntity;
using System.Collections.Generic;

namespace YariSavari.BusinessService
{
    public interface ILocationServices
    {
        LocationEntity GetLocationById(int locationId);
        IEnumerable<LocationEntity> GetAllLocations();
        int CreateLocation(LocationEntity locationEntity);
        bool UpdateLocation(int locationId, LocationEntity locationEntity);
        bool DeleteLocation(int locationId);
    }
}
