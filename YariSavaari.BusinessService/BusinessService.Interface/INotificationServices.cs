﻿using System;
using System.Collections.Generic;
using YariSavaari.BusinessEntity;

namespace YariSavaari.BusinessService
{
    public interface INotificationServices
    {
        List<PickedPassengerEntity> GetAllNotifications(string userId);
        bool ShareRequest(string userId, CoRideDetailsEntity rideRequest);
    }
}
