﻿using YariSavari.BusinessEntity;
using System;
using System.Collections.Generic;

namespace YariSavari.BusinessService
{
    public interface ICommuterServices
    {
        CommuterEntity GetCommuterById(int commuterId);
        IEnumerable<CommuterEntity> GetAllCommuters();
        Guid CreateCommuter(CommuterEntity commuterEntity);
        bool UpdateCommuter(int commuterId, CommuterEntity commuterEntity);
        bool DeleteCommuter(int commuterId);
    }
}
