﻿using YariSavari.BusinessEntity;
using System.Collections.Generic;

namespace YariSavari.BusinessService
{
    public interface IPathServices
    {
        PathEntity GetPathById(int pathId);
        IEnumerable<PathEntity> GetAllPaths();
        int CreatePath(PathEntity pathEntity);
        bool UpdatePath(int pathId, PathEntity pathEntity);
        bool DeletePath(int pathId);
    }
}
