﻿using System;
using System.Collections.Generic;
using YariSavaari.BusinessEntity;

namespace YariSavaari.BusinessService
{
    public interface IShareRideServices
    {
        //IEnumerable<ShareRideEntity> GetAllShareRides();
        string CreateShareRide(List<PathEntity> path, DateTime startAt, int vehicleId, string userId);
        List<PickedPassengerEntity> PickPassenger(string userId);
        //bool ShareRequest(string userId, CoRideDetailsEntity rideRequest);
    }
}
