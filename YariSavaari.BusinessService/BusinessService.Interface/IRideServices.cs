﻿using YariSavari.BusinessEntity;
using System;
using System.Collections.Generic;

namespace YariSavari.BusinessService
{
    public interface IRideServices
    {
        RideEntity GetRideById(int rideId);
        IEnumerable<RideEntity> GetAllRides();
        Guid CreateRide(RideEntity rideEntity);
        bool UpdateRide(int rideId, RideEntity rideEntity);
        bool DeleteRide(int rideId);
    }
}
