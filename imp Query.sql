SELECT P.PathId,L1.LocationId,L1.Name,L2.LocationId,L2.Name FROM Paths P INNER JOIN Locations L1
ON P.From_LocationId = L1.LocationId
INNER JOIN Locations L2 ON P.To_LocationId = L2.LocationId