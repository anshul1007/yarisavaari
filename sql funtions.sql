/****** Object:  StoredProcedure [dbo].[usp_BookRideNotifications]    Script Date: 10/23/2015 22:33:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_BookRideNotifications]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_BookRideNotifications]
GO
/****** Object:  StoredProcedure [dbo].[usp_PickPassengersForBookRide]    Script Date: 10/23/2015 22:33:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PickPassengersForBookRide]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PickPassengersForBookRide]
GO
/****** Object:  StoredProcedure [dbo].[usp_PickPassengersForShareRide]    Script Date: 10/23/2015 22:33:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PickPassengersForShareRide]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PickPassengersForShareRide]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShareRideNotifications]    Script Date: 10/23/2015 22:33:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ShareRideNotifications]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ShareRideNotifications]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_fnCalcDistance]    Script Date: 10/23/2015 22:33:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_fnCalcDistance]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[udf_fnCalcDistance]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_fnCalcMinimum]    Script Date: 10/23/2015 22:33:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_fnCalcMinimum]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[udf_fnCalcMinimum]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_fnCalcMinutes]    Script Date: 10/23/2015 22:33:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_fnCalcMinutes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[udf_fnCalcMinutes]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_fnCalcMinutes]    Script Date: 10/23/2015 22:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_fnCalcMinutes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[udf_fnCalcMinutes](@date1 DATETIME, 
                                      @date2 DATETIME) 
returns INT 
AS 
  BEGIN 
      RETURN ( ABS(DATEDIFF(mi, @date1, @date2)) ) 
  END ' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[udf_fnCalcMinimum]    Script Date: 10/23/2015 22:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_fnCalcMinimum]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [dbo].[udf_fnCalcMinimum] (
    @val1 INT
    ,@val2 INT
    )
RETURNS INT
AS
BEGIN
    DECLARE @Result INT

    IF @val1 > @val2
        SET @Result = @val2
    ELSE
        SET @Result = @val1

    RETURN @Result
END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[udf_fnCalcDistance]    Script Date: 10/23/2015 22:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_fnCalcDistance]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[udf_fnCalcDistance](@lat1 FLOAT, 
                                           @lon1 FLOAT, 
                                           @lat2 FLOAT, 
                                           @lon2 FLOAT) 
returns FLOAT 
AS 
  BEGIN 
      RETURN ( geography::Point(@lat1, @lon1, 
             4326) ).STDistance(geography::Point(@lat2, @lon2, 4326)) 
  END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ShareRideNotifications]    Script Date: 10/23/2015 22:33:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ShareRideNotifications]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_ShareRideNotifications] (@ShareRideId UNIQUEIDENTIFIER)
AS
BEGIN
    SELECT *
    FROM (
        SELECT B.BookRideId AS RideId
            ,S.StartAt
            ,P.PathId
            ,L1.LocationId AS FromId
            ,L1.NAME AS FromName
            ,L2.LocationId AS ToId
            ,L2.NAME AS ToName
            ,CASE 
                WHEN C.RideStatus = 2
                    AND C.RideType = 2
                    AND C.ShareRideId = @ShareRideId
                    THEN 3
                ELSE ISNULL(C.RideStatus, 1)
                END AS RideStatus
            ,CASE 
                WHEN C.RideStatus = 4
                    THEN U.FirstName + '' '' + U.LastName
                ELSE ''''
                END AS NAME
            ,CASE 
                WHEN C.RideStatus = 4
                    THEN U.PhoneNumber
                ELSE ''''
                END PhoneNumber
        FROM ShareRides S
        INNER JOIN Paths P ON P.ShareRide_ShareRideId = S.ShareRideId
            AND S.ShareRideId = @ShareRideId
            AND S.StartAt > GETDATE()
        INNER JOIN Locations L1 ON L1.LocationId = P.From_LocationId
            AND L1.LocationType = 1
        INNER JOIN Locations L2 ON L2.LocationId = P.To_LocationId
            AND L2.LocationType = 2
        INNER JOIN CoRideDetails C ON C.ShareRideId = S.ShareRideId
        INNER JOIN BookRides B ON B.BookRideId = C.BookRideId
        INNER JOIN AspNetUsers U ON U.Id = B.User_Id
        ) AS A
    WHERE A.RideStatus > 2
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_PickPassengersForShareRide]    Script Date: 10/23/2015 22:33:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PickPassengersForShareRide]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'    
CREATE PROCEDURE [dbo].[usp_PickPassengersForShareRide] (@ShareRideId uniqueidentifier)    
AS    
BEGIN
    DECLARE @Distance FLOAT = 9999999999999999999
    DECLARE @User_ID VARCHAR(50)
    DECLARE @PathId INT = 0
    DECLARE @TimeFlexibility INT = 0
        ,@PickAreaRange INT = 0
        ,@DestinationAreaRange INT = 0

    SELECT @TimeFlexibility = TimeFlexibility
        ,@PickAreaRange = PickAreaRange
        ,@DestinationAreaRange = DestinationAreaRange
        ,@User_ID = U.Id
    FROM AspNetUsers U
    INNER JOIN ShareRides S ON S.User_Id = U.Id
    WHERE S.ShareRideId = @ShareRideId

    DECLARE @Table TABLE (
        UserID UNIQUEIDENTIFIER
        ,PathID INT
        )

    WHILE (1 = 1)
    BEGIN
        SELECT TOP 1 @Distance = P.Distance
            ,@PathID = P.PathId
        FROM ShareRides S
        INNER JOIN Paths P ON P.ShareRide_ShareRideId = S.ShareRideId
        WHERE S.ShareRideId = @ShareRideId
            AND P.Distance < @Distance
        ORDER BY P.Distance DESC

        -- Exit loop if no more records    
        IF @@ROWCOUNT = 0
            BREAK;

        INSERT INTO @Table
        SELECT B.User_Id
            ,P1.PathID
        FROM BookRides B
        INNER JOIN AspNetUsers U ON U.Id = B.User_Id
            AND U.Id <> @User_ID
        INNER JOIN Paths P1 ON P1.BookRide_BookRideId = B.BookRideId
        INNER JOIN Locations BookFrom ON BookFrom.LocationId = P1.From_LocationId
        INNER JOIN Locations BookTo ON BookTo.LocationId = P1.To_LocationId
        INNER JOIN Paths P2 ON P2.PathId = @PathId
        INNER JOIN Locations ShareFrom ON ShareFrom.LocationId = P2.From_LocationId
        INNER JOIN Locations ShareTo ON ShareTo.LocationId = P2.To_LocationId
        WHERE B.IsActive = 1
            AND dbo.udf_fnCalcDistance(ShareFrom.Latitude, ShareFrom.Longitude, BookFrom.Latitude, BookFrom.Longitude) < dbo.udf_fnCalcMinimum(U.PickAreaRange, @PickAreaRange)
            AND dbo.udf_fnCalcMinutes(ShareFrom.TIME, BookFrom.TIME) < dbo.udf_fnCalcMinimum(U.TimeFlexibility, @TimeFlexibility)
            AND dbo.udf_fnCalcDistance(ShareTo.Latitude, ShareTo.Longitude, BookTo.Latitude, BookTo.Longitude) < dbo.udf_fnCalcMinimum(U.DestinationAreaRange, @DestinationAreaRange)
            AND B.StartAt > GETDATE()
        ORDER BY P1.Distance DESC

        IF (
                SELECT COUNT(UserID)
                FROM @Table
                ) > 10
            BREAK;
    END

    SELECT TOP 10 B.BookRideId AS RideId
        ,B.StartAt
        ,P.PathId
        ,L1.LocationId AS FromId
        ,L1.NAME AS FromName
        ,L2.LocationId AS ToId
        ,L2.NAME AS ToName
        ,CASE 
            WHEN C.RideStatus = 2 AND C.RideType = 2
                AND C.ShareRideId = @ShareRideId
                THEN 3
            ELSE ISNULL(C.RideStatus, 1)
            END AS RideStatus
        ,CASE 
            WHEN C.RideStatus = 4
                THEN U.FirstName + '' '' + U.LastName
            ELSE ''''
            END AS NAME
        ,CASE 
            WHEN C.RideStatus = 4
                THEN U.PhoneNumber
            ELSE ''''
            END PhoneNumber
    FROM BookRides B
    INNER JOIN @Table t ON B.User_Id = t.UserID
    INNER JOIN Paths P ON P.BookRide_BookRideId = B.BookRideId
        AND t.PathID = P.PathId
    INNER JOIN Locations L1 ON L1.LocationId = P.From_LocationId
    INNER JOIN Locations L2 ON L2.LocationId = P.To_LocationId
    INNER JOIN AspNetUsers U ON U.Id = B.User_Id
    LEFT JOIN CoRideDetails C ON C.BookRideId = B.BookRideId
        AND C.ShareRideId = @ShareRideId
    ORDER BY B.BookRideId
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_PickPassengersForBookRide]    Script Date: 10/23/2015 22:33:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PickPassengersForBookRide]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_PickPassengersForBookRide] (@BookRideId UNIQUEIDENTIFIER)
AS
BEGIN
    DECLARE @Distance FLOAT = 9999999999999999999
    DECLARE @User_ID VARCHAR(50)
    DECLARE @PathId INT = 0
    DECLARE @TimeFlexibility INT = 0
        ,@PickAreaRange INT = 0
        ,@DestinationAreaRange INT = 0

    SELECT @TimeFlexibility = TimeFlexibility
        ,@PickAreaRange = PickAreaRange
        ,@DestinationAreaRange = DestinationAreaRange
        ,@User_ID = U.Id
    FROM AspNetUsers U
    INNER JOIN BookRides B ON B.User_Id = U.Id
    WHERE B.BookRideId = @BookRideId

    DECLARE @Table TABLE (
        UserID UNIQUEIDENTIFIER
        ,PathID INT
        )

    WHILE (1 = 1)
    BEGIN
        SELECT TOP 1 @Distance = P.Distance
            ,@PathID = P.PathId
        FROM BookRides B
        INNER JOIN Paths P ON P.BookRide_BookRideId = B.BookRideId
        WHERE B.BookRideId = @BookRideId
            AND P.Distance < @Distance
        ORDER BY P.Distance DESC

        -- Exit loop if no more records    
        IF @@ROWCOUNT = 0
            BREAK;

        INSERT INTO @Table
        SELECT S.User_Id
            ,P1.PathID
        FROM ShareRides S
        INNER JOIN AspNetUsers U ON U.Id = S.User_Id
            AND U.Id <> @User_ID
        INNER JOIN Paths P1 ON P1.ShareRide_ShareRideId = S.ShareRideId
        INNER JOIN Locations BookFrom ON BookFrom.LocationId = P1.From_LocationId
        INNER JOIN Locations BookTo ON BookTo.LocationId = P1.To_LocationId
        INNER JOIN Paths P2 ON P2.PathId = @PathId
        INNER JOIN Locations ShareFrom ON ShareFrom.LocationId = P2.From_LocationId
        INNER JOIN Locations ShareTo ON ShareTo.LocationId = P2.To_LocationId
        WHERE S.IsActive = 1
            AND dbo.udf_fnCalcDistance(ShareFrom.Latitude, ShareFrom.Longitude, BookFrom.Latitude, BookFrom.Longitude) < dbo.udf_fnCalcMinimum(U.PickAreaRange, @PickAreaRange)
            AND dbo.udf_fnCalcMinutes(ShareFrom.TIME, BookFrom.TIME) < dbo.udf_fnCalcMinimum(U.TimeFlexibility, @TimeFlexibility)
            AND dbo.udf_fnCalcDistance(ShareTo.Latitude, ShareTo.Longitude, BookTo.Latitude, BookTo.Longitude) < dbo.udf_fnCalcMinimum(U.DestinationAreaRange, @DestinationAreaRange)
            AND S.StartAt > GETDATE()
        ORDER BY P1.Distance DESC

        IF (
                SELECT COUNT(UserID)
                FROM @Table
                ) > 10
            BREAK;
    END

    SELECT TOP 10 S.ShareRideId AS RideId
        ,S.StartAt
        ,P.PathId
        ,L1.LocationId AS FromId
        ,L1.NAME AS FromName
        ,L2.LocationId AS ToId
        ,L2.NAME AS ToName
        ,CASE 
           WHEN C.RideStatus = 2 AND C.RideType = 1
                AND C.BookRideId = @BookRideId
                THEN 3
            ELSE ISNULL(C.RideStatus, 1)
            END AS RideStatus
        ,CASE 
            WHEN C.RideStatus = 4
                THEN U.FirstName + '' '' + U.LastName
            ELSE ''''
            END AS NAME
        ,CASE 
            WHEN C.RideStatus = 4
                THEN U.PhoneNumber
            ELSE ''''
            END PhoneNumber
    FROM ShareRides S
    INNER JOIN @Table t ON S.User_Id = t.UserID
    INNER JOIN Paths P ON P.ShareRide_ShareRideId = S.ShareRideId
        AND t.PathID = P.PathId
    INNER JOIN Locations L1 ON L1.LocationId = P.From_LocationId
    INNER JOIN Locations L2 ON L2.LocationId = P.To_LocationId
    INNER JOIN AspNetUsers U ON U.Id = S.User_Id
    LEFT JOIN CoRideDetails C ON C.BookRideId = @BookRideId
        AND C.ShareRideId = S.ShareRideId
    ORDER BY S.ShareRideId
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_BookRideNotifications]    Script Date: 10/23/2015 22:33:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_BookRideNotifications]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_BookRideNotifications] (@BookRideId UNIQUEIDENTIFIER)
AS
BEGIN
    SELECT *
    FROM (
        SELECT S.ShareRideId AS RideId
            ,B.StartAt
            ,P.PathId
            ,L1.LocationId AS FromId
            ,L1.NAME AS FromName
            ,L2.LocationId AS ToId
            ,L2.NAME AS ToName
            ,CASE 
                WHEN C.RideStatus = 2
                    AND C.RideType = 1
                    AND C.BookRideId = @BookRideId
                    THEN 3
                ELSE ISNULL(C.RideStatus, 1)
                END AS RideStatus
            ,CASE 
                WHEN C.RideStatus = 4
                    THEN U.FirstName + '' '' + U.LastName
                ELSE ''''
                END AS NAME
            ,CASE 
                WHEN C.RideStatus = 4
                    THEN U.PhoneNumber
                ELSE ''''
                END PhoneNumber
        FROM BookRides B
        INNER JOIN Paths P ON P.BookRide_BookRideId = B.BookRideId
            AND B.BookRideId = @BookRideId
            AND B.StartAt > GETDATE()
        INNER JOIN Locations L1 ON L1.LocationId = P.From_LocationId
        INNER JOIN Locations L2 ON L2.LocationId = P.To_LocationId
        INNER JOIN CoRideDetails C ON C.BookRideId = B.BookRideId
        INNER JOIN ShareRides S ON S.ShareRideId = C.ShareRideId
        INNER JOIN AspNetUsers U ON U.Id = S.User_Id
        ) AS A
    WHERE A.RideStatus > 2
END
' 
END
GO
