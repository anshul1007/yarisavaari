﻿using System.ComponentModel.Composition;
using System.Data.Entity;
using YariSavaari.Resolver;
using YariSavaari.DataModel.UnitOfWork;

namespace YariSavaari.DataModel
{
    [Export(typeof(IComponent))]
    public class DependencyResolver : IComponent
    {
        public void SetUp(IRegisterComponent registerComponent)
        {
            registerComponent.RegisterType<IUnitOfWork, UnitOfWork.UnitOfWork>();
        }
    }
}
