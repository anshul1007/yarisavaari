﻿#region Using Namespaces...

using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using YariSavaari.DataModel.Repository;

#endregion

namespace YariSavaari.DataModel.UnitOfWork
{
    /// <summary>
    /// Unit of Work class responsible for DB transactions
    /// </summary>
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        #region Private member variables...

        private YariSavaariDBContext _context = null;
        private GenericRepository<ApplicationUser> _applicationUserRepository;
        private GenericRepository<Vehicle> _vehicleRepository;
        private GenericRepository<Path> _pathRepository;
        private GenericRepository<Location> _locationRepository;
        private GenericRepository<BookRide> _bookRideRepository;
        private GenericRepository<ShareRide> _shareRideRepository;
        private GenericRepository<CoRideDetails> _coRideDetailsRepository;

        #endregion

        public UnitOfWork()
        {
            _context = new YariSavaariDBContext();
        }

        #region Public Repository Creation properties...

        /// <summary>
        /// Get/Set Property for ApplicationUser repository.
        /// </summary>
        public GenericRepository<ApplicationUser> ApplicationUserRepository
        {
            get
            {
                if (this._applicationUserRepository == null)
                    this._applicationUserRepository = new GenericRepository<ApplicationUser>(_context);
                return _applicationUserRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for vehicle repository.
        /// </summary>
        public GenericRepository<Vehicle> VehicleRepository
        {
            get
            {
                if (this._vehicleRepository == null)
                    this._vehicleRepository = new GenericRepository<Vehicle>(_context);
                return _vehicleRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for path repository.
        /// </summary>
        public GenericRepository<Path> PathRepository
        {
            get
            {
                if (this._pathRepository == null)
                    this._pathRepository = new GenericRepository<Path>(_context);
                return _pathRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for location repository.
        /// </summary>
        public GenericRepository<Location> LocationRepository
        {
            get
            {
                if (this._locationRepository == null)
                    this._locationRepository = new GenericRepository<Location>(_context);
                return _locationRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for ride repository.
        /// </summary>
        public GenericRepository<BookRide> BookRideRepository
        {
            get
            {
                if (this._bookRideRepository == null)
                    this._bookRideRepository = new GenericRepository<BookRide>(_context);
                return _bookRideRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for commuter repository.
        /// </summary>
        public GenericRepository<ShareRide> ShareRideRepository
        {
            get
            {
                if (this._shareRideRepository == null)
                    this._shareRideRepository = new GenericRepository<ShareRide>(_context);
                return _shareRideRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for ride repository.
        /// </summary>
        public GenericRepository<CoRideDetails> CoRideDetailsRepository
        {
            get
            {
                if (this._coRideDetailsRepository == null)
                    this._coRideDetailsRepository = new GenericRepository<CoRideDetails>(_context);
                return _coRideDetailsRepository;
            }
        }
        #endregion

        #region Public member methods...
        /// <summary>
        /// Save method.
        /// </summary>
        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                var outputLines = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.Add(string.Format(
                        "{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:", DateTime.Now,
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                System.IO.File.AppendAllLines(@"C:\errors.txt", outputLines);

                throw e;
            }

        }

        #endregion

        #region Implementing IDiosposable...

        #region private dispose variable declaration...
        private bool disposed = false;
        #endregion

        /// <summary>
        /// Protected Virtual Dispose method
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}