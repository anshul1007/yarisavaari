namespace YariSavaari.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Locations", "BookRide_BookRideId", "dbo.BookRides");
            DropForeignKey("dbo.Locations", "ShareRide_ShareRideId", "dbo.ShareRides");
            DropIndex("dbo.Locations", new[] { "BookRide_BookRideId" });
            DropIndex("dbo.Locations", new[] { "ShareRide_ShareRideId" });
            DropColumn("dbo.Locations", "BookRide_BookRideId");
            DropColumn("dbo.Locations", "ShareRide_ShareRideId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Locations", "ShareRide_ShareRideId", c => c.Guid());
            AddColumn("dbo.Locations", "BookRide_BookRideId", c => c.Guid());
            CreateIndex("dbo.Locations", "ShareRide_ShareRideId");
            CreateIndex("dbo.Locations", "BookRide_BookRideId");
            AddForeignKey("dbo.Locations", "ShareRide_ShareRideId", "dbo.ShareRides", "ShareRideId");
            AddForeignKey("dbo.Locations", "BookRide_BookRideId", "dbo.BookRides", "BookRideId");
        }
    }
}
