namespace YariSavaari.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BookRides",
                c => new
                    {
                        BookRideId = c.Guid(nullable: false),
                        StartAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        IsActive = c.Boolean(nullable: false),
                        ShareRide_ShareRideId = c.Guid(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.BookRideId)
                .ForeignKey("dbo.ShareRides", t => t.ShareRide_ShareRideId)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.ShareRide_ShareRideId)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        LocationId = c.Int(nullable: false, identity: true),
                        LocationType = c.Int(nullable: false),
                        Name = c.String(),
                        Longitude = c.String(),
                        Latitude = c.String(),
                        Time = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        BookRide_BookRideId = c.Guid(),
                        ShareRide_ShareRideId = c.Guid(),
                    })
                .PrimaryKey(t => t.LocationId)
                .ForeignKey("dbo.BookRides", t => t.BookRide_BookRideId)
                .ForeignKey("dbo.ShareRides", t => t.ShareRide_ShareRideId)
                .Index(t => t.BookRide_BookRideId)
                .Index(t => t.ShareRide_ShareRideId);
            
            CreateTable(
                "dbo.Paths",
                c => new
                    {
                        PathId = c.Int(nullable: false, identity: true),
                        Distance = c.Double(nullable: false),
                        From_LocationId = c.Int(),
                        To_LocationId = c.Int(),
                        BookRide_BookRideId = c.Guid(),
                        ShareRide_ShareRideId = c.Guid(),
                    })
                .PrimaryKey(t => t.PathId)
                .ForeignKey("dbo.Locations", t => t.From_LocationId)
                .ForeignKey("dbo.Locations", t => t.To_LocationId)
                .ForeignKey("dbo.BookRides", t => t.BookRide_BookRideId)
                .ForeignKey("dbo.ShareRides", t => t.ShareRide_ShareRideId)
                .Index(t => t.From_LocationId)
                .Index(t => t.To_LocationId)
                .Index(t => t.BookRide_BookRideId)
                .Index(t => t.ShareRide_ShareRideId);
            
            CreateTable(
                "dbo.ShareRides",
                c => new
                    {
                        ShareRideId = c.Guid(nullable: false),
                        StartAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        IsActive = c.Boolean(nullable: false),
                        User_Id = c.String(maxLength: 128),
                        Vehicle_VehicleId = c.Int(),
                    })
                .PrimaryKey(t => t.ShareRideId)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .ForeignKey("dbo.Vehicles", t => t.Vehicle_VehicleId)
                .Index(t => t.User_Id)
                .Index(t => t.Vehicle_VehicleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        JoinDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Gender = c.Int(nullable: false),
                        TimeFlexibility = c.Int(nullable: false),
                        PickAreaRange = c.Int(nullable: false),
                        DestinationAreaRange = c.Int(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Vehicles",
                c => new
                    {
                        VehicleId = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        RegistrationNo = c.String(),
                        VehicleType = c.Int(nullable: false),
                        Make = c.String(),
                        Model = c.String(),
                        Color = c.String(),
                        Capacity = c.Int(nullable: false),
                        OnlyFemaleCommuter = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.VehicleId);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Secret = c.String(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        ApplicationType = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        RefreshTokenLifeTime = c.Int(nullable: false),
                        AllowedOrigin = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RefreshTokens",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Subject = c.String(nullable: false, maxLength: 50),
                        ClientId = c.String(nullable: false, maxLength: 50),
                        IssuedUtc = c.DateTime(nullable: false),
                        ExpiresUtc = c.DateTime(nullable: false),
                        ProtectedTicket = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.BookRides", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ShareRides", "Vehicle_VehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.ShareRides", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Paths", "ShareRide_ShareRideId", "dbo.ShareRides");
            DropForeignKey("dbo.Locations", "ShareRide_ShareRideId", "dbo.ShareRides");
            DropForeignKey("dbo.BookRides", "ShareRide_ShareRideId", "dbo.ShareRides");
            DropForeignKey("dbo.Paths", "BookRide_BookRideId", "dbo.BookRides");
            DropForeignKey("dbo.Paths", "To_LocationId", "dbo.Locations");
            DropForeignKey("dbo.Paths", "From_LocationId", "dbo.Locations");
            DropForeignKey("dbo.Locations", "BookRide_BookRideId", "dbo.BookRides");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.ShareRides", new[] { "Vehicle_VehicleId" });
            DropIndex("dbo.ShareRides", new[] { "User_Id" });
            DropIndex("dbo.Paths", new[] { "ShareRide_ShareRideId" });
            DropIndex("dbo.Paths", new[] { "BookRide_BookRideId" });
            DropIndex("dbo.Paths", new[] { "To_LocationId" });
            DropIndex("dbo.Paths", new[] { "From_LocationId" });
            DropIndex("dbo.Locations", new[] { "ShareRide_ShareRideId" });
            DropIndex("dbo.Locations", new[] { "BookRide_BookRideId" });
            DropIndex("dbo.BookRides", new[] { "User_Id" });
            DropIndex("dbo.BookRides", new[] { "ShareRide_ShareRideId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.RefreshTokens");
            DropTable("dbo.Clients");
            DropTable("dbo.Vehicles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.ShareRides");
            DropTable("dbo.Paths");
            DropTable("dbo.Locations");
            DropTable("dbo.BookRides");
        }
    }
}
