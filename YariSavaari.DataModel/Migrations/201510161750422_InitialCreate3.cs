namespace YariSavaari.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CoRideDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SenderRideId = c.String(),
                        RecipientRideId = c.String(),
                        RideType = c.Int(nullable: false),
                        RideStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CoRideDetails");
        }
    }
}
