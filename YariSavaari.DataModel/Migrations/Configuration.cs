using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using YariSavaari.DataModel;
using YariSavaari.Utility;

namespace YariSavaari.DataModel.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<YariSavaariDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(YariSavaariDBContext context)
        {
            //ScalarFunctionContextInitializer(context.Database);

            if (context.Clients.Count() > 0)
            {
                return;
            }

            context.Clients.AddRange(BuildClientsList());
            context.SaveChanges();
        }

//        private void ScalarFunctionContextInitializer(Database db)
//        {
//            // SELECT  [dbo].[udf_fnCalcDistance](77.597022199999969, 13.0357695,77.584707999999978,13.014422)

//            db.ExecuteSqlCommand(@"IF EXISTS (SELECT * 
//           FROM   sys.objects 
//           WHERE  object_id = Object_id(N'[dbo].[udf_fnCalcDistance]') 
//                  AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' )) 
//  DROP FUNCTION [dbo].[udf_fnCalcDistance]");

//            db.ExecuteSqlCommand(@"CREATE FUNCTION [dbo].[udf_fnCalcDistance](@lat1 FLOAT, 
//                                           @lon1 FLOAT, 
//                                           @lat2 FLOAT, 
//                                           @lon2 FLOAT) 
//returns FLOAT 
//AS 
//  BEGIN 
//      RETURN ( geography::Point(@lat1, @lon1, 
//             4326) ).STDistance(geography::Point(@lat2, @lon2, 4326)) 
//  END");

//            db.ExecuteSqlCommand(@"IF EXISTS (SELECT * 
//           FROM   sys.objects 
//           WHERE  object_id = Object_id(N'[dbo].[udf_fnCalcMinutes]') 
//                  AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' )) 
//  DROP FUNCTION [dbo].[udf_fnCalcMinutes]");

//            db.ExecuteSqlCommand(@"CREATE FUNCTION [dbo].[udf_fnCalcMinutes](@date1 DATETIME, 
//                                      @date2 DATETIME) 
//returns INT 
//AS 
//  BEGIN 
//      RETURN ( ABS(DATEDIFF(mi, @date1, @date2)) ) 
//  END ");

//        }

        private static List<Client> BuildClientsList()
        {

            List<Client> ClientsList = new List<Client> 
            {
                new Client
                { Id = "ngAuthApp", 
                    Secret= Helper.GetHash("abc@123"), 
                    Name="AngularJS front-end Application", 
                    ApplicationType =  ApplicationTypes.JavaScript, 
                    Active = true, 
                    RefreshTokenLifeTime = 7200, 
                    AllowedOrigin = "http://localhost:803"
                },
                new Client
                { Id = "consoleApp", 
                    Secret=Helper.GetHash("123@abc"), 
                    Name="Console Application", 
                    ApplicationType = ApplicationTypes.NativeConfidential, 
                    Active = true, 
                    RefreshTokenLifeTime = 14400, 
                    AllowedOrigin = "*"
                }
            };

            return ClientsList;
        }
    }
}
