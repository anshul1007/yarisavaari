namespace YariSavaari.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CoRideDetails", "ShareRideId", c => c.String());
            AddColumn("dbo.CoRideDetails", "BookRideId", c => c.String());
            DropColumn("dbo.CoRideDetails", "SenderRideId");
            DropColumn("dbo.CoRideDetails", "RecipientRideId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CoRideDetails", "RecipientRideId", c => c.String());
            AddColumn("dbo.CoRideDetails", "SenderRideId", c => c.String());
            DropColumn("dbo.CoRideDetails", "BookRideId");
            DropColumn("dbo.CoRideDetails", "ShareRideId");
        }
    }
}
