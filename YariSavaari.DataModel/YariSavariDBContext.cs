﻿using CodeFirstStoreFunctions;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YariSavaari.DataModel
{

    //enable-migrations
    //add-migration InitialCreate
    //update-database

    public class YariSavaariDBContext : IdentityDbContext<ApplicationUser>
    {
        public YariSavaariDBContext()
            : base("name=YariSavaariDBConnectionString", throwIfV1Schema: false)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public static YariSavaariDBContext Create()
        {
            return new YariSavaariDBContext();
        }

        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Path> Paths { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<BookRide> BookRides { get; set; }
        public DbSet<ShareRide> ShareRides { get; set; }
        public DbSet<CoRideDetails> CoRideDetails { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            ////Configure domain classes using Fluent API here
            //modelBuilder.Entity<Vehicle>()
            //.Property(p => p.UserId)
            //.HasColumnName("ApplicationUser_Id");
            modelBuilder.Conventions.Add(new FunctionsConvention<YariSavaariDBContext>("dbo"));

            //cmd.ExecuteNonQuery();
            //cmd.Connection.Close();

            base.OnModelCreating(modelBuilder);
        }

        //[DbFunction("CodeFirstDatabaseSchema", "udf_fnCalcDistance")]
        //public static double CalcDistance(double lat1, double lon1, double lat2, double lon2)
        //{
        //    throw new NotSupportedException();
        //}

        //[DbFunction("CodeFirstDatabaseSchema", "udf_fnCalcMinutes")]
        //public static int CalcMinutes(DateTime date1, DateTime date2)
        //{
        //    throw new NotSupportedException();
        //}

        //[DbFunction("MultipleResultSetsContext", "usp_PickPassengers")]
        //[DbFunctionDetails(ResultTypes = new[] { typeof(RideSuggestion) })]
        //public virtual ObjectResult<RideSuggestion> PickPassengers([ParameterType(typeof(string))] ObjectParameter userId)
        //{
        //    return ((IObjectContextAdapter)this).ObjectContext
        //        .ExecuteFunction<RideSuggestion>("usp_PickPassengers", userId);
        //}
    }
}
