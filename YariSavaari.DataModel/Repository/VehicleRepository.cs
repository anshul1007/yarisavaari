﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YariSavari.DataModel.Repository
{
    public class VehicleRepository
    {
        private YariSavariDBContext _ctx;

        public VehicleRepository()
        {
            _ctx = new YariSavariDBContext();
        }

        public int CreateVehicle(Vehicle vehicle, ApplicationUser user)
        {
            vehicle.UserId = user.Id;

            var vehicle1 = _ctx.Vehicles.Add(vehicle);
            _ctx.SaveChanges();

            return vehicle1.VehicleId;

        }
    }
}
