﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace YariSavaari.DataModel
{
    public class Path
    {
        public int PathId { get; set; }
        public virtual Location From { get; set; }
        public virtual Location To { get; set; }
        public double Distance { get; set; }
        public virtual BookRide BookRide { get; set; }
        public virtual ShareRide ShareRide { get; set; }
    }

}
