﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using YariSavaari.Utility;

namespace YariSavaari.DataModel
{
    public class Location
    {
        public int LocationId { get; set; }
        public LocationType LocationType { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime Time { get; set; }
    }
}
