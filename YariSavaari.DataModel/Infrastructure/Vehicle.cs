﻿namespace YariSavaari.DataModel
{
    public class Vehicle
    {
        public int VehicleId { get; set; }
        public string UserId { get; set; }
        public string RegistrationNo { get; set; }
        public int VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public int Capacity { get; set; }
        public bool OnlyFemaleCommuter { get; set; }
    }
}
