﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YariSavaari.DataModel.Infrastructure
{
    public class BookInfo
    {
        public Guid BookId { get; set; }

        public virtual ApplicationUser User { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string ISBN { get; set; }

        public double MaxPrice { get; set; }

        public double OfferPrice { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime RegisteredAt { get; set; }

        public bool IsActive { get; set; }
    }
}
