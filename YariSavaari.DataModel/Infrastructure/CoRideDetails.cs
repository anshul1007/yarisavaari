﻿using YariSavaari.Utility;

namespace YariSavaari.DataModel
{
    public class CoRideDetails
    {
        public int Id { get; set; }
        public string ShareRideId { get; set; }
        public string BookRideId { get; set; }
        public RideType RideType { get; set; }
        public RideStatus RideStatus { get; set; }
    }
}
