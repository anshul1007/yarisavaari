﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace YariSavaari.DataModel
{
    public class BookRide
    {
        public BookRide()
        {
            //Location = new List<Location>();
            Path = new List<Path>();
        }

        public Guid BookRideId { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual ShareRide ShareRide { get; set; }

        //public List<Location> Location { get; set; }

        public virtual List<Path> Path { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartAt { get; set; }

        public bool IsActive { get; set; }

    }
}
