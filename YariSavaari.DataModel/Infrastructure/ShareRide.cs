﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace YariSavaari.DataModel
{
    public class ShareRide
    {
        public ShareRide()
        {
            //this.Location = new List<Location>();
            this.Path = new List<Path>();
            this.BookRide = new List<BookRide>();
        }

        public Guid ShareRideId { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual Vehicle Vehicle { get; set; }

        //public List<Location> Location { get; set; }

        public virtual List<Path> Path { get; set; }

        public virtual List<BookRide> BookRide { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartAt { get; set; }

        public bool IsActive { get; set; }

        //public bool SendRequestToAll { get; set; }
        //public int Status { get; set; } // request send, conform etc, 
    }
}
