﻿
namespace YariSavaari.Utility
{
    public enum LocationType
    {
        Source = 1,
        Destination = 2,
        Via = 3,
    }

    public enum RideType
    {
        Share = 1,
        Book = 2
    }

    public enum RideStatus
    {
        Send = 1,
        Sent = 2,
        Pending = 3,
        Accepted = 4,
        Declined = 5,
        Canceled = 6
    }

    public enum ApplicationTypes
    {
        JavaScript = 0,
        NativeConfidential = 1
    };
}
