﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace YariSavaari.Utility
{
    public static class Function
    {
        const string _JavaScriptKey = "AIzaSyCbxN444pi4ct1-nl8b6iqu1L_pKrfWZgY";

        public static int GetMin(int x, int y)
        {
            return x < y ? x : y;
        }
        
        //public static Expression<Func<double, double, double, double, double>> GetDistance = 
        //    (fromLatitude, fromLongitude, toLatitude, toLongitude) =>
        //    (new GeoCoordinate(fromLatitude, fromLongitude)).GetDistanceTo(new GeoCoordinate(toLatitude, toLongitude));

        public static int GetTime(DateTime x, DateTime y)
        {
            var timeSpan = DateTime.Compare(x, y) <= 0 ? y - x : x - y;
            return (int)timeSpan.TotalMinutes;
        }

        public static double GetDistance(double fromLatitude, double fromLongitude, double toLatitude, double toLongitude)
        {
            var refFrom = new GeoCoordinate(fromLatitude, fromLongitude);
            var refTo = new GeoCoordinate(toLatitude, toLongitude);
            return refFrom.GetDistanceTo(refTo);
        }

        public static int GetAprroxTime(string fromGeo, string toGeo)
        {
            int time = 0;
            using (var client = new HttpClient())
            {
                var url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + fromGeo + "&destinations=" + toGeo + "&sensor=false&key=" + _JavaScriptKey;
                var response = client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    // by calling .Result you are performing a synchronous call
                    var responseContent = response.Content;
                    // by calling .Result you are synchronously reading the result
                    string responseString = responseContent.ReadAsStringAsync().Result;
                    dynamic stuff = JObject.Parse(responseString);
                    time = stuff.rows[0].elements[0].duration.value;
                }
            }
            return time;
        }
    }
}
