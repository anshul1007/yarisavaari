﻿using YariSavaari.Utility;

namespace YariSavaari.BusinessEntity
{
    public class CoRideDetailsEntity
    {
        public string ShareRideId { get; set; }
        public string BookRideId { get; set; }
        public RideType RideType { get; set; }
        public RideStatus RideStatus { get; set; }
    }
}
