﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YariSavaari.Utility;

namespace YariSavaari.BusinessEntity
{
    public class RideSuggestionEntity
    {
        public Guid RideId { get; set; }
        public DateTime StartAt { get; set; }
        public int PathId { get; set; }
        public int FromId { get; set; }
        public string FromName { get; set; }
        public int ToId { get; set; }
        public string ToName { get; set; }
        public RideStatus RideStatus { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class PickedPassengerEntity
    {
        public Guid SenderId { get; set; }
        public DateTime StartAt { get; set; }
        public int FromId { get; set; }
        public string FromName { get; set; }
        public int ToId { get; set; }
        public string ToName { get; set; }
        public List<RideSuggestionEntity> RideSuggestionEntity { get; set; }
        public RideType RideType { get; set; }
        public PickedPassengerEntity()
        {
            RideSuggestionEntity = new List<RideSuggestionEntity>();
        }
    }
}
