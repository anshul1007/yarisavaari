﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace YariSavaari.BusinessEntity
{
    public class BookRideEntity
    {
        public BookRideEntity()
        {
            //Location = new List<LocationEntity>();
            Path = new List<PathEntity>();
        }

        public Guid BookRideId { get; set; }

        public ApplicationUserEntity User { get; set; }

        public ShareRideEntity ShareRide { get; set; }

        //public List<LocationEntity> Location { get; set; }

        public List<PathEntity> Path { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartAt { get; set; }

        public bool IsActive { get; set; }
    }
}
