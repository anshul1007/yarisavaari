﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace YariSavaari.BusinessEntity
{
    public class ApplicationUserEntity : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime JoinDate { get; set; }
        public int Gender { get; set; }
        public int TimeFlexibility { get; set; }
        public int PickAreaRange { get; set; }
        public int DestinationAreaRange { get; set; }
        //public virtual IList<VehicleEntity> Car { get; set; }
    }
}
