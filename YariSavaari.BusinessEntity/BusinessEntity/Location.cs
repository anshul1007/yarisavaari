﻿using System;
using YariSavaari.Utility;

namespace YariSavaari.BusinessEntity
{
    public class LocationEntity
    {
        public int LocationId { get; set; }
        public LocationType LocationType { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime Time { get; set; }
    }
}
