﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace YariSavaari.BusinessEntity
{
    public class ShareRideEntity
    {
        public ShareRideEntity()
        {
            //this.Location = new List<LocationEntity>();
            this.Path = new List<PathEntity>();
            this.BookRide = new List<BookRideEntity>();
        }

        public Guid ShareRideId { get; set; }

        public ApplicationUserEntity User { get; set; }

        public VehicleEntity Vehicle { get; set; }

        //public List<LocationEntity> Location { get; set; }

        public List<PathEntity> Path { get; set; }

        public List<BookRideEntity> BookRide { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartAt { get; set; }

        public bool IsActive { get; set; }

        //public bool SendRequestToAll { get; set; }
        //public int Status { get; set; } // request send, conform etc, 
    }
}
