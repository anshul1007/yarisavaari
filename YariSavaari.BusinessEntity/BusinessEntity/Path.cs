﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace YariSavaari.BusinessEntity
{
    public class PathEntity
    {
        public int PathId { get; set; }
        public LocationEntity From { get; set; }
        public LocationEntity To { get; set; }
        public double Distance { get; set; }
    }

}
