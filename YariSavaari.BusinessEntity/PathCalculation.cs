﻿using System;
using System.Collections.Generic;
using System.Linq;
using YariSavaari.Utility;

namespace YariSavaari.BusinessEntity
{
    public static class PathGeneration
    {
        public static List<PathEntity> GeneratePath(List<LocationEntity> locations)
        {
            List<PathEntity> path = new List<PathEntity>();
            locations = locations.OrderBy(x => x.Time).ToList();

            var l = 0;
            foreach (var loc in locations)
            {
                for (var j = l + 1; j < locations.Count; j++)
                {
                    path.Add(new PathEntity()
                    {
                        From = loc,
                        To = locations[j],
                        Distance = Function.GetDistance(loc.Latitude, loc.Longitude, locations[j].Latitude, locations[j].Longitude)
                    });
                }
                l++;
            }

            return path;
        }

        public static void CalculateTime(List<LocationEntity> locations, DateTime startTime)
        {
            if (locations == null
                || !locations.Any(s => s.LocationType == LocationType.Source)
                || !locations.Any(d => d.LocationType == LocationType.Destination))
            {
                throw new Exception("locations are invalid incorrect");
            }

            var source = locations.Where(s => s.LocationType == LocationType.Source).FirstOrDefault();
            var sourceGeo = source.Latitude + "," + source.Longitude;
            var sourceTime = startTime;
            source.Time = sourceTime;

            var destination = locations.Where(s => s.LocationType == LocationType.Destination).FirstOrDefault();
            var destinationGeo = destination.Latitude + "," + destination.Longitude;
            var destinationTime = sourceTime.AddSeconds(Function.GetAprroxTime(sourceGeo, destinationGeo));
            destination.Time = destinationTime;

            var vias = locations.Where(s => s.LocationType == LocationType.Via);
            if (vias != null && vias.Any())
            {
                foreach (var via in vias)
                {
                    var viaGeo = via.Latitude + "," + via.Longitude;
                    var viaTime = sourceTime.AddSeconds(Function.GetAprroxTime(sourceGeo, viaGeo));
                    via.Time = viaTime;
                }
            }
        }
    }
}
